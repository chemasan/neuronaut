#include "neuroplot.hpp"
#include <cmath>
#include <cstring>
#include <cstdio>
#include <cstdint>
#include <stdexcept>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

namespace neuroplot
{

using namespace std;

// ----------------------------------------  Private functions ----------------------------------------  

bool compare(float fval1, float fval2, uint32_t precision = 10)
{
	long long multiplier = pow(10,precision);
	long long ival1 = round( fval1 * multiplier);
	long long ival2 = round( fval2 * multiplier);
	return ival1 == ival2;
}

SDL_Surface* createSurface(int32_t width, int32_t height)
{
	//TODO: review proper byte ordering
	Uint32 rmask = 0x00ff0000;
	Uint32 gmask = 0x0000ff00;
	Uint32 bmask = 0x000000ff;
	Uint32 amask = 0xff000000;
	SDL_Surface* surface = SDL_CreateRGBSurface(SDL_SWSURFACE|SDL_SRCCOLORKEY, width, height, 32, rmask,gmask,bmask,amask);
	if (surface == nullptr) throw runtime_error("Failed creating surface");
	SDL_SetColorKey(surface, SDL_SRCCOLORKEY, SDL_MapRGB( surface->format, 0, 0xFF, 0xFF ) );
	return surface;
}

SDL_Surface* copySurface(SDL_Surface* surface)
{
	auto flags = SDL_SWSURFACE;
	if (surface->flags & SDL_SRCCOLORKEY) flags = flags | SDL_SRCCOLORKEY;
	if (surface->flags & SDL_SRCALPHA) flags = flags | SDL_SRCALPHA;
	SDL_Surface* ret = SDL_CreateRGBSurface(flags, surface->w, surface->h, surface->format->BitsPerPixel, surface->format->Rmask, surface->format->Gmask, surface->format->Bmask, surface->format->Amask);
	if (SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0)
		{
			SDL_FreeSurface(ret);
			throw runtime_error("Failed locking surface");
		}
	}
	if (SDL_MUSTLOCK(ret))
	{
		if (SDL_LockSurface(ret) < 0)
		{
			if (SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
			SDL_FreeSurface(ret);
			throw runtime_error("Failed locking surface");
		}
	}
	memcpy(ret->pixels,surface->pixels, ret->w * ret->h * sizeof(uint32_t));
	if (SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
	if (SDL_MUSTLOCK(ret)) SDL_UnlockSurface(ret);
	if (surface->flags & SDL_SRCCOLORKEY) SDL_SetColorKey(ret, SDL_RLEACCEL|SDL_SRCCOLORKEY, surface->format->colorkey);
	return ret;
}

void applySurface(int16_t x, int16_t y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = nullptr)
{
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;
	SDL_BlitSurface( source, clip, destination, &offset );
}

uint32_t getPixel32(const SDL_Surface* surface, int32_t x, int32_t y)
{
	if(x < 0 || y < 0 || x >= surface->w || y >= surface->h) throw out_of_range("Requested pixel outside surface bounds");
	const Uint32* pixels = (const Uint32*)surface->pixels;  //Convert the pixels to 32 bit
	return pixels[ (y*surface->w) + x ];					//Get the pixel requested
}

void setPixel32(SDL_Surface* surface, int32_t x, int32_t y, uint32_t pixel)
{
	if(x < 0 || y < 0 || x >= surface->w || y >= surface->h) return;
	Uint32* pixels = (Uint32*) surface->pixels;
	pixels[(y * surface->w) + x] = pixel;
}

SDL_Surface* flipSurface32(SDL_Surface* surface, bool verticalFlip, bool horitzontalFlip)
{
	auto flags = SDL_SWSURFACE;
	if (surface->flags & SDL_SRCCOLORKEY) flags = flags | SDL_SRCCOLORKEY;
	if (surface->flags & SDL_SRCALPHA) flags = flags | SDL_SRCALPHA;
	SDL_Surface* flipped = SDL_CreateRGBSurface(flags, surface->w, surface->h, surface->format->BitsPerPixel, surface->format->Rmask, surface->format->Gmask, surface->format->Bmask, surface->format->Amask);

	// Lock surface for direct pixel access if the surfaces requires it
	if (SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0)
		{
			SDL_FreeSurface(flipped);
			throw runtime_error("Failed locking surface");
		}
	}
	if (SDL_MUSTLOCK(flipped))
	{
		if (SDL_LockSurface(flipped) < 0)
		{
			if (SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
			SDL_FreeSurface(flipped);
			throw runtime_error("Failed locking surface");
		}
	}

	// Copy pixels
	for (int32_t x = 0, rx = flipped->w - 1;  x < flipped->w;  x++, rx--)
	{
		for (int32_t y = 0, ry = flipped->h - 1;  y < flipped->h;  y++, ry--)
		{
			uint32_t pixel = getPixel32(surface, x, y);
			if (verticalFlip && horitzontalFlip)   setPixel32(flipped, rx, ry, pixel);
			else if (horitzontalFlip)   setPixel32(flipped, rx, y, pixel);
			else if (verticalFlip)	  setPixel32(flipped, x, ry, pixel);
		}
	}

	// Unlock the previously locked surfaces
	if (SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
	if (SDL_MUSTLOCK(flipped)) SDL_UnlockSurface(flipped);
	// Copy color key when aplicable
	if (surface->flags & SDL_SRCCOLORKEY) SDL_SetColorKey(flipped, SDL_RLEACCEL|SDL_SRCCOLORKEY, surface->format->colorkey);

	return flipped;
}

// ----------------------------------------  Public functions ----------------------------------------  

template<typename T>
double distance(T x1, T y1, T x2, T y2)
{
	return sqrt( pow(x2 - x1, 2) + pow(y2 - y1, 2) );
}
double distance(PointInt p1, PointInt p2)
{
	return distance(p1.x, p1.y, p2.x, p2.y);
}
double distance(PointFloat p1, PointFloat p2)
{
	return distance(p1.x, p1.y, p2.x, p2.y);
}

// ----------------------------------------  Color class ----------------------------------------  

Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) : red(r), green(g), blue(b), alpha(a)
{
	// Nothing to do
}
Color::Color(uint32_t pixel)
{
	alpha = (pixel & 0xff000000) >> 24;
	red   = (pixel & 0x00ff0000) >> 16;
	green = (pixel & 0x0000ff00) >> 8;
	blue  = (pixel & 0x000000ff);
}
uint32_t Color::toInt32() const
{
	uint32_t ret = alpha;
	ret = (ret<<8) | red;
	ret = (ret<<8) | green;
	ret = (ret<<8) | blue;
	return ret;
}

// ----------------------------------------  Surface class ----------------------------------------  

// Pixel and image manipulations
void Surface::apply(const Surface& other, int16_t x, int16_t y)
{
	applySurface(x, y, (SDL_Surface*) other.data, (SDL_Surface*) data);
}
void Surface::apply(const Surface& other, int16_t srcX, int16_t srcY, uint16_t srcWidth, uint16_t srcHeight, int16_t x, int16_t y)
{
	SDL_Rect clipArea{srcX, srcY, srcWidth, srcHeight};
	applySurface(x, y, (SDL_Surface*) other.data, (SDL_Surface*)data, &clipArea);
}
uint32_t Surface::getPixel(int32_t x, int32_t y)
{
	SDL_Surface* surface = (SDL_Surface*) data;
	if(x < 0 || y < 0 || x >= surface->w || y >= surface->h) throw out_of_range("Requested pixel outside surface bounds");
	if(SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0) throw runtime_error("Failed locking surface");
	}
	auto ret = getPixel32(surface, x, y);
	if(SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
	return ret;
}
void Surface::setPixel(int32_t x, int32_t y, uint32_t pixel)
{
	auto surface = (SDL_Surface*) data;
	if(x < 0 || y < 0 || x >= surface->w || y >= surface->h) throw out_of_range("Requested pixel outside surface bounds");
	if(SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0) throw runtime_error("Failed locking surface");
	}
	setPixel32(surface, x, y, pixel);
	if(SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
}
void Surface::fill(const Color& color)
{
	auto surface = (SDL_Surface*) data;
	SDL_FillRect(surface, nullptr, color.toInt32());
}
void Surface::rectangle(int16_t x, int16_t y, uint16_t width, uint16_t height, const Color& color)
{
	auto surface = (SDL_Surface*) data;
	SDL_Rect rect{x, y, width, height};
	SDL_FillRect(surface, &rect, color.toInt32());
}
void Surface::circle(int32_t cx, int32_t cy, int32_t radius, const Color& color)
{
	radius = abs(radius);
	uint32_t pixel = color.toInt32();
	auto surface = (SDL_Surface*) data;
	if(SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0) throw runtime_error("Failed locking surface");
	}
	for (int32_t x = -radius; x < radius; x++)
	{
		for (int32_t y = -radius; y < radius; y++)
		{
			int32_t pixelx = cx + x;
			int32_t pixely = cy + y;
			if(pixelx < 0 || pixelx >= surface->w || pixely < 0 || pixely >= surface->h)  continue;
			int32_t dist = distance(pixelx, pixely, cx, cy);
			if(dist < radius) setPixel32(surface, pixelx, pixely, pixel);
		}
	}
	if(SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
}
void Surface::vline(int32_t x, const Color& color, uint32_t thick)
{
	uint32_t pixel = color.toInt32();
	thick = thick/2;
	auto surface = (SDL_Surface*) data;
	if(SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0) throw runtime_error("Failed locking surface");
	}
	for (int32_t y = 0; y < surface->h; y++)
	{
		setPixel32(surface, x, y, pixel);
		for (int32_t lx = x - thick; lx <= x + thick; lx++)
		{
			for (int32_t ly = y - thick; ly <= y + thick; ly++)
			{
				if (distance(x, y, lx, ly) <= thick) setPixel32(surface, lx, ly, pixel);
			}
		}		
	}
	if(SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
}
void Surface::hline(int32_t y, const Color& color, uint32_t thick)
{
	uint32_t pixel = color.toInt32();
	thick = thick/2;
	auto surface = (SDL_Surface*) data;
	if(SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0) throw runtime_error("Failed locking surface");
	}
	for (int32_t x = 0; x < surface->w; x++)
	{
		setPixel32(surface, x, y, pixel);
		for (int32_t lx = x - thick; lx <= x + thick; lx++)
		{
			for (int32_t ly = y - thick; ly <= y + thick; ly++)
			{
				if (distance(x, y, lx, ly) <= thick) setPixel32(surface, lx, ly, pixel);
			}
		}		
	}
	if(SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
}
void Surface::line(const PointInt& p1, const PointInt& p2, const Color& color, uint32_t thick)
{
	uint32_t pixel = color.toInt32();
	thick = thick/2;
	auto surface = (SDL_Surface*) data;
	if(SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0) throw runtime_error("Failed locking surface");
	}

	// Ref: https://en.wikipedia.org/wiki/Digital_differential_analyzer_(graphics_algorithm)
	float dx = p2.x - p1.x;
	float dy = p2.y - p1.y;
	float step = max(abs(dx),abs(dy));
	dx = dx / step;
	dy = dy / step;
	float x = p1.x;
	float y = p1.y;
	for (int32_t i = 1; i <= step; i++)
	{
		setPixel32(surface, round(x), round(y), pixel);
		for (int32_t lx = x - thick; lx <= x + thick; lx++)
		{
			for (int32_t ly = y - thick; ly <= y + thick; ly++)
			{
				if (distance(x, y, (float)lx, (float)ly) <= thick) setPixel32(surface, lx, ly, pixel);
			}
		}
		x = x + dx;
		y = y + dy;
	}

	if(SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
}

Surface Surface::flip(bool vertically, bool horizontally)
{
	SDL_Surface* ret = flipSurface32( (SDL_Surface*) data, vertically, horizontally);
	return Surface(ret);
}
Surface Surface::clip(int16_t x, int16_t y, uint16_t width, uint16_t height)
{
	Surface clipped(width, height);
	clipped.apply(*this, x, y, width, height);
	return clipped;
}
void Surface::setColorKey(const Color& color)
{
	SDL_Surface* surface = (SDL_Surface*) data;
	if (SDL_SetColorKey(surface, SDL_SRCCOLORKEY, color.toInt32()) == -1) throw runtime_error("Failed setting surface color key");
}
void Surface::clearColorKey()
{
	SDL_Surface* surface = (SDL_Surface*) data;
	if (SDL_SetColorKey(surface, 0, 0) == -1) throw runtime_error("Failed setting surface color key");
}

// Surface information
int32_t Surface::width() const
{
	SDL_Surface* surface = (SDL_Surface*) data;
	return surface->w;
}
int32_t Surface::height() const
{
	SDL_Surface* surface = (SDL_Surface*) data;
	return surface->h;
}

// Operators
Surface& Surface::operator=(const Surface& other)
{
	SDL_Surface* otherSurface = (SDL_Surface*) other.data;
	SDL_Surface* thisData = copySurface(otherSurface);
	if (data != nullptr) SDL_FreeSurface((SDL_Surface*)data);
	data = thisData;
	return *this;
}
Surface& Surface::operator=(Surface&& other)
{
	swap(other.data, data);
	return *this;
}

// Constructors & destructors
Surface::Surface(int32_t width, int32_t height)
{
	data = createSurface(width,height);
}
Surface::Surface(int32_t width, int32_t height, const uint32_t* pixels) : Surface(width, height)
{
	auto surface = (SDL_Surface*) data;
	if(SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0) throw runtime_error("Failed locking surface");
	}
	memcpy(surface->pixels, pixels, width * height * sizeof(uint32_t));
	if(SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
}
Surface::Surface() : Surface(1,1)
{
	// Nothing to do
}
Surface::Surface(const Surface& other)
{
	SDL_Surface* otherSurface = (SDL_Surface*) other.data;
	data = copySurface(otherSurface);
}
Surface::Surface(Surface&& other)
{
	data = other.data;
	other.data = nullptr;
}
Surface::~Surface()
{
	if (data != nullptr) SDL_FreeSurface((SDL_Surface*)data);
}
Surface::Surface(void* data) : data(data)
{
	// Nothing to do
}

// ----------------------------------------  TrueTypeFont ----------------------------------------  

// Text rendering
Surface TrueTypeFont::render(const string& text, const Color& color)
{
	SDL_Surface* surface = TTF_RenderText_Solid( (TTF_Font*) data, text.c_str(), SDL_Color{color.red, color.green, color.blue, color.alpha});
	return Surface(surface);
}

// Builders
TrueTypeFont TrueTypeFont::loadFile(const string& filepath, int32_t size)
{
	TTF_Font* font = TTF_OpenFont(filepath.c_str(), size);
	if( font == nullptr ) throw runtime_error("Failed loading font");
	return TrueTypeFont(font);
}

// Operators
TrueTypeFont& TrueTypeFont::operator=(TrueTypeFont&& other)
{
	swap(other.data, data);
	return *this;
}

// Constructors and destructors
TrueTypeFont::TrueTypeFont(TrueTypeFont&& other)
{
	swap(other.data, data);
}
TrueTypeFont::~TrueTypeFont()
{
	if(data!= nullptr) TTF_CloseFont((TTF_Font*) data);
}

TrueTypeFont::TrueTypeFont(void* data) : data(data)
{
	// Nothing to do
}


// ----------------------------------------  Plot class ----------------------------------------  

void Plot::apply(const Surface& other, float x, float y)
{
	surface.apply(other, translateX(x), translateY(y) - other.height());
}
void Plot::text(const string& text, float x, float y, const Color& color)
{
	if (!font) throw runtime_error("No font loaded");
	auto label = font->render(text, color);
	apply(label, x, y);
}
void Plot::drawGrid(float stepx, float stepy, const Color& color, uint32_t thick)
{
	for(float x = 0; x <= maxx; x+= stepx)  vline(x, color, thick);
	for(float x = 0; x >= minx; x-= stepx)  vline(x, color, thick);
	for(float y = 0; y <= maxy; y+= stepy)  hline(y, color, thick);
	for(float y = 0; y >= miny; y-= stepy)  hline(y, color, thick);
}
void Plot::drawAxis(float stepx, float stepy, const Color& color, uint32_t thick)
{
	thick = thick >= 1 ? thick : 1;
	vline(0, color, thick);
	hline(0, color, thick);
	if (stepx > 0 && stepy > 0)
	{
		for (float x = 0; x <= maxx; x+=stepx) line(x, stepy/10, x, -(stepy/10), color, thick);
		for (float x = 0; x >= minx; x-=stepx) line(x, stepy/10, x, -(stepy/10), color, thick);
		for (float y = 0; y <= maxy; y+=stepy) line(-(stepx/10), y, (stepx/10), y, color, thick);
		for (float y = 0; y >= miny; y-=stepy) line(-(stepx/10), y, (stepx/10), y, color, thick);
	}
}
void Plot::fill(const Color& color)
{
	surface.fill(color);
}
void Plot::rectangle(float x, float y, float width, float height, const Color& color)
{
	surface.rectangle(translateX(x),translateY(y+height), translateWidth(width) +1 , translateHeight(height) +1 , color);
}
void Plot::circle(float x, float y, float radius, const Color& color)
{
	//TODO: reimplement considering that the circle in the surface may be eliptic 
	surface.circle(translateX(x), translateY(y), translateWidth(radius), color);
}
void Plot::vline(float x, const Color& color, uint32_t thick)
{
	surface.vline(translateX(x), color, thick);
}
void Plot::hline(float y, const Color& color, uint32_t thick)
{
	surface.hline(translateY(y), color, thick);
}
void Plot::line(float x1, float y1, float x2, float y2, const Color& color, uint32_t thick)
{
	surface.line(PointInt(translateX(x1),translateY(y1)), PointInt(translateX(x2),translateY(y2)), color, thick);
}
void Plot::line(const PointFloat& p1, const PointFloat& p2, const Color& color, uint32_t thick)
{
	auto x1 = translateX(p1.x);
	auto y1 = translateY(p1.y);
	auto x2 = translateX(p2.x);
	auto y2 = translateY(p2.y);
	surface.line(PointInt(x1,y1), PointInt(x2,y2), color, thick);
}


void Plot::points(const vector<float>& points, const Color& color, float radius)
{
	for (float x = 0; x <= maxx && x < points.size(); x++) circle(x, points[x], radius, color);
}
void Plot::points(const vector<PointFloat>& points, const Color& color, float radius)
{
	for(const PointFloat& p: points) circle(p.x, p.y, radius, color );
}
void Plot::points(const map<float,float>& points, const Color& color, float radius)
{
	for (const auto& i : points) circle(i.first,i.second, radius, color);
}
void Plot::points(function<float(float)> f, float step, const Color& color, float radius)
{
	step = abs(step);
	for (float x = minx; x <= maxx; x+=step) circle(x, f(x), radius, color);
}


void Plot::lines(const vector<float>& points, const Color& color, uint32_t thick)
{
	float prevx = 0;
	for (float x = 1; x <= maxx && x < points.size(); x++)
	{
		line(prevx, points[prevx], x, points[x], color, thick);
		prevx=x;
	}
}
void Plot::lines(const vector<PointFloat>& points, const Color& color, uint32_t thick)
{
	auto it = points.begin();
	PointFloat prev = *it;
	it++;
	for (; it != points.end(); it++)
	{
		line(prev, *it, color, thick);
		prev = *it;
	}
}
void Plot::lines(const map<float,float>& points, const Color& color, uint32_t thick)
{
	auto it = points.begin();
	float prevx = it->first;
	float prevy = it->second;
	for (; it != points.end(); it++)
	{
		line(prevx, prevy, it->first, it->second, color, thick);
		prevx = it->first;
		prevy = it->second;
	}
}
void Plot::lines(function<float(float)> f, float step, const Color& color, uint32_t thick)
{
	step = abs(step);
	float prevx = minx-step;
	float prevy = f(prevx);
	for (float x = minx; x <= maxx+step; x+=step)
	{
		float y = f(x);
		if(isfinite(x) && isfinite(y) && isfinite(prevx) && isfinite(prevy) )
		{
			if (y < miny) y = miny;
			if (y > maxy) y = maxy;
			line(prevx, prevy, x, y, color, thick);
		}
		prevx = x;
		prevy = y;
	}
}


void Plot::gradient(const vector<vector<float>>& pixels, const Color& from, const Color& to, const Color& colorStep)
{
	float maxz = pixels[0][0];
	float minz = pixels[0][0];
	for (size_t x = 0; x < pixels.size() && x < maxx; x++)
	{
		for (size_t y = 0; y < pixels[x].size() && y < maxy; y++)
		{
			maxz = max(maxz,pixels[x][y]);
			minz = min(minz,pixels[x][y]);
		}
	}
	for (size_t x = 0; x < pixels.size() && x < maxx; x++)
	{
		for (size_t y = 0; y < pixels[x].size() && y < maxy; y++)
		{
			float z = pixels[x][y];
			z = ((z-minz) * (float)0xff) / (maxz - minz);
			uint8_t r, g, b;
			r = g = b = (uint8_t)z;
			r = (r * (to.red - from.red))/0xff;
			g = (g * (to.green - from.green))/0xff;
			b = (b * (to.blue - from.blue))/0xff;
			r += from.red;
			g += from.green;
			b += from.blue;
			r -= colorStep.red > 0 ? r % colorStep.red : 0;
			g -= colorStep.green > 0 ? g % colorStep.green : 0;
			b -= colorStep.blue > 0 ? b % colorStep.blue : 0;
			rectangle(x,y,1,1, {r,g,b});
		}
	}
}

void Plot::gradient(function<float(float,float)> f, float step, const Color& from, const Color& to, const Color& colorStep)
{
	step = abs(step);
	float maxz = f(minx,miny);
	float minz = f(minx,miny);
	for (float x = minx; x <= maxx; x+=step)
	{
		for (float y = miny; y <= maxy; y+=step)
		{
			maxz = max(maxz,f(x,y));
			minz = min(minz,f(x,y));
		}
	}
	for (float x = minx; x <= maxx; x+=step)
	{
		for (float y = miny;  y <= maxy; y+=step)
		{
			float z = f(x,y);
			z = ((z-minz) * (float)0xff) / (maxz - minz);
			uint8_t r, g, b;
			r = g = b = (uint8_t)z;
			r = (r * (to.red - from.red))/0xff;
			g = (g * (to.green - from.green))/0xff;
			b = (b * (to.blue - from.blue))/0xff;
			r += from.red;
			g += from.green;
			b += from.blue;
			r -= colorStep.red > 0 ? r % colorStep.red : 0;
			g -= colorStep.green > 0 ? g % colorStep.green : 0;
			b -= colorStep.blue > 0 ? b % colorStep.blue : 0;
			rectangle(x,y,step,step, {r,g,b});
		}
	}
}
void Plot::gradient(function<float(float,float)> f, float step, float minz, float maxz, const Color& from, const Color& to, const Color& colorStep)
{
	step = abs(step);
	for (float x = minx; x <= maxx; x+=step)
	{
		for (float y = miny;  y <= maxy; y+=step)
		{
			float z = f(x,y);
			z = ((z-minz) * (float)0xff) / (maxz - minz);
			uint8_t r, g, b;
			r = g = b = (uint8_t)z;
			r = (r * (to.red - from.red))/0xff;
			g = (g * (to.green - from.green))/0xff;
			b = (b * (to.blue - from.blue))/0xff;
			r += from.red;
			g += from.green;
			b += from.blue;
			r -= colorStep.red > 0 ? r % colorStep.red : 0;
			g -= colorStep.green > 0 ? g % colorStep.green : 0;
			b -= colorStep.blue > 0 ? b % colorStep.blue : 0;
			rectangle(x,y,step,step, {r,g,b});
		}
	}
}


Plot::Plot(uint32_t width, uint32_t height, float minx, float maxx, float miny, float maxy) : surface(width, height)
{
	this->minx = min(minx,maxx);
	this->maxx = max(maxx,minx);
	this->miny = min(miny,maxy);
	this->maxy = max(miny,maxy);
	fill(WHITE);
}
Plot::Plot(uint32_t width, uint32_t height, float minx, float maxx, float miny, float maxy, const string& fontFile, uint32_t fontSize) : Plot(width, height, minx, maxx, miny, maxy)
{
	font = TrueTypeFont::loadFile(fontFile, fontSize);
}


int32_t Plot::translateX(float x)
{
	return ((x-minx) *  (float)surface.width()) / (maxx - minx);
}
int32_t Plot::translateY(float y)
{
	return surface.height() - (((y-miny) * (float)surface.height()) / (maxy - miny));
}
int32_t Plot::translateWidth(float width)
{
	return (width * surface.width()) / (maxx - minx);
}
int32_t Plot::translateHeight(float height)
{
	return (height * surface.height()) / (maxy - miny);
}



// ----------------------------------------  Display class ----------------------------------------  

void Display::resize(int32_t width, int32_t height)
{
	auto flags = fullScreen ? SDL_SWSURFACE | SDL_FULLSCREEN : SDL_SWSURFACE;
	screen.data = SDL_SetVideoMode(width, height, 32, flags);
	if(screen.data == nullptr) throw runtime_error("Failed setting video mode");
}
void Display::setFullscreen(bool fullscreen)
{
	if (!this->fullScreen && fullscreen)
	{
		screen.data = SDL_SetVideoMode(width(), height(), 32, SDL_SWSURFACE | SDL_FULLSCREEN);
		if(screen.data == nullptr) throw runtime_error("Failed setting video mode");
		this->fullScreen = true;
	}
	else if (this->fullScreen && !fullscreen)
	{
		screen.data = SDL_SetVideoMode(width(), height(), 32, SDL_SWSURFACE);
		if(screen.data == nullptr) throw runtime_error("Failed setting video mode");
		this->fullScreen = true;
	}
}
bool Display::getFullScreen() const
{
	return fullScreen;
}
void Display::setCaption(const string& caption)
{
	SDL_WM_SetCaption(caption.c_str(), nullptr);   
}
void Display::refresh()
{
	SDL_Surface* screenSurface = (SDL_Surface*) screen.data;
	if( SDL_Flip( screenSurface ) == -1 ) throw runtime_error("Failed update the screen");
}

// Screen surface
void Display::apply(const Surface& other, int16_t x, int16_t y)
{
	screen.apply(other, x, y);
}
void Display::apply(const Surface& other, int16_t srcX, int16_t srcY, uint16_t srcWidth, uint16_t srcHeight, int16_t x, int16_t y)
{
	screen.apply(other, srcX, srcY, srcWidth, srcHeight, x, y);
}
void Display::apply(const Plot& plot, int16_t x, int16_t y)
{
	screen.apply(plot.surface, x, y);
}
void Display::apply(const Plot& plot, int16_t srcX, int16_t srcY, uint16_t srcWidth, uint16_t srcHeight, int16_t x, int16_t y)
{
	screen.apply(plot.surface, srcX, srcY, srcWidth, srcHeight, x, y);
}


void Display::fill(const Color& color)
{
	screen.fill(color);
}

int32_t Display::width() const
{
	return screen.width();
}
int32_t Display::height() const
{
	return screen.height();
}

// Constructors and destructors
Display::Display(const string caption, int32_t width, int32_t height, bool fullscreen)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)  throw runtime_error("Error initializing SDL");
	if( TTF_Init() == -1 ) throw runtime_error("Failed to initialize TTF");
	auto flags = fullscreen ? SDL_SWSURFACE | SDL_FULLSCREEN : SDL_SWSURFACE;
	SDL_Surface* surface = SDL_SetVideoMode(width, height, 32, flags);
	if (surface == nullptr) throw runtime_error("Failed initializing display");
	screen = Surface(surface);
	setCaption(caption);
}
Display::~Display()
{
	TTF_Quit();
	screen.data = nullptr;  // prevent the display surface from being freed by SDL_FreeSurface as it is cleaned up by SDL_Quit
	SDL_Quit();
}


}
