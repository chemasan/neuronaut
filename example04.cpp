#include "neuronaut.hpp"
#include <iostream>

/*
	Example of saving a network to a file and loading a network from a file
*/

using namespace std;
using namespace neuronaut;

int main()
{
	vector<ActivationFunction> activations = {
		linear, linear, linear,							   // Input layer
		relu, relu,										   // Hidden layer 0
		sigmoid, sigmoid, sigmoid, sigmoid,				   // Hidden layer 1
		neuronaut::tanh, neuronaut::tanh, neuronaut::tanh,	// Hidden layer 2
		binaryStep, signum									// Output layer
	};
	vector<float> biases = {
		1,2,3,	 // Input layer
		4,5,	   // Hidden layer 0
		6,7,8,9,   // Hidden layer 1
		10,11,12,  // Hidden layer 2
		13,14	  // Output layer
	};
	vector<float> weights = {
		1,2,  3,4,  5,6,						  // Input to hidden 0
		7,8,9,10, 11,12,13,14,					// Hidden 0 to hidden 1
		15,16,17, 18,19,20, 21,22,23, 24,25,26,   // Hidden 1 to hidden 2
		27,28, 29,30, 31,32					   // Hidden 2 to output
	};

	Network net = buildNetwork({3, 2,4,3, 2}, activations, biases, weights);
	save(net, "/tmp/prueba.neural");

	Network recovered = load("/tmp/prueba.neural");
	cout << "Inputs:  " << recovered.layers.front().size() << endl;
	cout << "Outputs: " << recovered.layers.back().size() << endl;
	cout << "Layers:  " << recovered.layers.size() << " --> ";

	for (auto& l : recovered.layers) cout << " (" << l.size() << ")";
	cout << endl;

	cout << "Biases: ";
	for (auto& b : recovered.getBias()) cout << " " << b;
	cout << endl;

	cout << "Weights: ";
	for (auto& w : recovered.getWeights()) cout << " " << w;
	cout << endl;

	cout << "Activation functions: ";
	for (auto& f : recovered.getActivations()) cout << " " << toString(f);
	cout << endl;

	return 0;
}
