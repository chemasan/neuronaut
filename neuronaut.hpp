#ifndef __NEURONAUT_HPP
#define __NEURONAUT_HPP

#include <functional>
#include <random>
#include <vector>
#include <map>
#include <set>


namespace neuronaut
{

using namespace std;

// ##### Auxiliar functions and classess #####

class Random
{
public:
	int getInt(int min, int max);
	int operator()(int min, int max);
	float getFloat(float min, float max);
	float operator()(float min, float max);
	template<typename T>
	T get(T min, T max)
	{
		uniform_int_distribution<T> dist(min,max);
		return dist(rng);
	}

	Random(unsigned int seed);
	Random();

private:
	mt19937 rng;
};

string toString(time_t t);
vector<float> randomFloats(size_t amount, float min, float max);
vector<float> slice(const vector<float>& v, size_t length, size_t offset = 0);
map< vector<float>,vector<float> > normalize(map<float,float> v);
vector< map< vector<float>,vector<float> > > spread(map< vector<float>,vector<float> > v, size_t partitions);
string toString(const vector<float>& v);
ostream& operator<<(ostream& out,const vector<float>& v);

template<class T>
T sum(vector<T> v)
{
	T result = 0;
	for (T value : v) result += value;
	return result;
}

template<class T>
T avg(vector<T> v)
{
	return sum(v) / v.size();
}

bool isIn(char c, const string& s);
string ltrim(const string& str, string toremove = " \n\r\v\t");
string rtrim(const string& str, string toremove = " \n\r\v\t");
string trim(const string& str, string toremove = " \n\r\v\t");

vector<vector<float>> loadFloatCsv(const string& filePath, char fieldDelimiter = ',', float fillValue = 0);
map<vector<float>,vector<float>> loadSamples(const string& filePath, set<size_t> keys = {0}, char fieldDelimiter = ',', float fillValue = 0);

// ##### Activation functions #####

using ActivationFunction = function<float(float)>;

bool operator==(const ActivationFunction& fo, float(*fp)(float));
bool operator==(const ActivationFunction& f1, const ActivationFunction& f2);

float linear(float value);
float relu(float value);
float sigmoid(float value);
float softplus(float value);
float tanh(float value);

float linearDerivative(float x);
float reluDerivative(float x);
float sigmoidDerivative(float x);
float softplusDerivative(float x);
float tanhDerivative(float x);

// ##### Loss functions #####
using LossFunction = function<float(float,float)>;

bool operator==(const LossFunction& fo, float(*fp)(float,float));
bool operator==(const LossFunction& f1, const LossFunction& f2);

float mse(float expected, float obtained);
float mseDerivative(float expected, float obtained);
float halfMse(float expected, float obtained);
float halfMseDerivative(float expected, float obtained);

// ##### Other useful mathematical functions #####
float binaryStep(float value);
float signum(float value);

ActivationFunction getDerivative(const ActivationFunction& f);
string toString(const ActivationFunction& funk);

// ##### Neuron and Network #####

class Neuron
{
public:
	float input = 0;
	float output = 0;
	float bias = 0;
	vector<float> weights;
	ActivationFunction activation = linear;
	ActivationFunction activationDerivative = linearDerivative;

	float delta = 0;
	float deltaAcummulated = 0;
	vector<float> weightsDeltas;
	vector<float> weightsDeltasAccumulated;

	Neuron(ActivationFunction activation, float bias = 0);
	Neuron(const Neuron& rhs);
	Neuron(Neuron&& rhs);

	Neuron& operator =(const Neuron& rhs);
	Neuron& operator =(Neuron&& rhs) noexcept;
};

using Layer = vector<Neuron>;
using Layers = vector<Layer>;


class Network
{
public:
	Layers layers;
	size_t accumulated = 0;

	vector<float> forwardPropagation(const vector<float>& inputValues);
	void setBias(const vector<float>& biases);
	void setWeights(const vector<float>& weights);
	void setActivations(vector<ActivationFunction>);
	vector<float> getBias() const;
	vector<float> getWeights() const;
	vector<ActivationFunction> getActivations() const;

	void backPropagation(const vector<float>& expected, const vector<float>& results);
	void gradientDescend(float learningRate);
	void trainBatch(float learningRate, const vector<pair<vector<float> ,vector<float>>>& samples);

	Network();
	Network(const Network& rhs);
	Network(Network&& rhs);

	Network& operator =(const Network& rhs) noexcept;
	Network& operator =(Network&& rhs) noexcept;
};

size_t calculateNeuronsAmount(const vector<size_t>& topology);
size_t calculateConnectionsAmount(const vector<size_t>& topology);

Network buildNetwork(const vector<size_t>& topology, ActivationFunction activation);
Network buildNetwork(const vector<size_t>& topology, ActivationFunction activation, const vector<float>& biases, const vector<float>& weights);
Network buildNetwork(const vector<size_t>& topology, const vector<ActivationFunction>& activation, const vector<float>& biases, const vector<float>& weights);
Network initNetwork(const vector<size_t>& topology, ActivationFunction activation, float minWeight = -1, float maxWeight = 1);
Network initNetwork(const vector<size_t>& topology, const vector<ActivationFunction>& activationFunctions, float minWeight = -1, float maxWeight = 1);

void save(Network& net, const string& filePath);
Network load(const string& filePath);
void print(Network& net);

// ##### Trainning #####

float calcLoss(Network& net, const map<float, float>& samples);
float calcLoss(Network& net, const map<vector<float>, vector<float>>& samples);


class DumbTrainning
{
public:
	float calcLoss();
	float trainIteration(float learningRate, float initloss);
	DumbTrainning(Network& net, const map< vector<float>,vector<float> >& samples);

private:
	Network& net;
	const map< vector<float>,vector<float> >& samples;
	float tuneWeight(Neuron& neuron, size_t connection, float learningRate, float initloss);
	float tuneBias(Neuron& neuron, float learningRate, float initloss);
	float tuneNeuron(Neuron& neuron, float learningRate, float initloss);
	float tuneLayer(Layer& layer, float learningRate, float initloss);
	float tuneNetwork(float learningRate, float initloss);
};

class GradientDescendTrainning
{
public:
	size_t epoch = 0;
	size_t iteration = 0;
	void trainIteration(float learningRate, size_t batchSize = 1);
	GradientDescendTrainning(Network& net, const map< vector<float>,vector<float> >& samples);

private:
	Network& net;
	const map< vector<float>,vector<float> >& originalSamples;
	vector< pair< vector<float>, vector<float> > > samples;
	size_t cursample = 0;
};

} // namespace

#endif // __NEURONAUT_HPP
