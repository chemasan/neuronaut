.PHONY: run clean cleandeps deps test examples all
.PRECIOUS: %.o
.DEFAULT_GOAL:=run
CXX:=clang++

%.o: %.cpp
	$(CXX) -std=c++20 -g -Ibuild/include -c $<

unittests: unittests.o neuronaut.o
	$(CXX) -std=c++20 -g -o $@ $^

example%.out: example%.o neuronaut.o neuroplot.o
	$(CXX) -std=c++20 -g -o $@ $^ -lSDL -lSDL_ttf

examples: $(patsubst %.cpp,%.out,$(wildcard example??.cpp))

all: unittests examples

run: example07.out
	./example07.out

test: unittests
	./unittests

clean:
	rm -f ./example*.out  unittests *.o

cleandeps:
	rm -f -r ./build/

deps:
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/catch-2.4.0.yaml"
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/backward-1.4.yaml"
