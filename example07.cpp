#include "neuronaut.hpp"
#include "neuroplot.hpp"
#include <iostream>
#include <csignal>
#include <chrono>
#include <thread>

using namespace std;
using namespace neuronaut;
using namespace neuroplot;

/*
	Example of creating a network, training it with sample data, and represent the outputs in a plot.
	Once trained, the network should return outputs matching the sample data.
	In this case we expect the network's output to predict the color of a given point.
*/

// Samples provided for trainning. For each pair, the first value (the map's key) is the network's input,
// and the second value (the map's value) is the expected output for the given input.
map< vector<float>, vector<float>> samples = loadSamples("sampledata/spiral2.csv",{0,1});
//map< vector<float>, vector<float>> samples = loadSamples("sampledata/circles.csv",{0,1});


// Signal handler to catch CRTL+C
bool keepRunning = true;
void sigHandler(int signum)
{
	keepRunning = false;
}

int main(int argc, char** argv)
{
	ios::sync_with_stdio(false);
	signal(SIGINT, sigHandler);

	Network net = initNetwork({2, 10,10,5, 1}, neuronaut::tanh);
	net.layers.back()[0].activation = neuronaut::tanh;
	net.layers.back()[0].activationDerivative = neuronaut::tanhDerivative;

	float initloss = calcLoss(net, samples);
	cerr << "Initial loss: " << initloss << endl ;

	Display display("example");
	Plot plot(display.width(), display.height(), -10, 10.01, -10.01, 10);
	plot.gradient([&](float x, float y){return net.forwardPropagation({x,y})[0];}, 0.4, BLUE, RED);
	plot.drawGrid();
	plot.drawAxis();
	for (auto& sample : samples)
	{
		plot.circle(sample.first[0], sample.first[1], 0.3, BLACK);
		plot.circle(sample.first[0], sample.first[1], 0.2, sample.second[0] > 0 ? RED : BLUE);
	}
	display.apply(plot);
	display.refresh();
	this_thread::sleep_for(chrono::milliseconds(500));

	GradientDescendTrainning t(net, samples);
	float lastloss = initloss;
	float loss = initloss;
	auto startTime = chrono::steady_clock::now();
	auto lastRefresh = startTime;
	while (keepRunning && loss > 0.001)
	{
		auto iterStartTime = chrono::steady_clock::now();
		t.trainIteration(0.003, 10);
		chrono::nanoseconds iterDuration = chrono::steady_clock::now() - iterStartTime;
		chrono::milliseconds refreshMillis = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - lastRefresh);
		if (refreshMillis.count() > 2000)
		{
			loss = calcLoss(net, samples);
			cerr << "iteration: " << t.iteration << ", epoch: " << t.epoch << ", loss: " <<  loss << ", iterDuration: " << iterDuration.count() << endl;
			plot.gradient([&](float x, float y){return net.forwardPropagation({x,y})[0];}, 0.4, BLUE, RED);
			plot.drawGrid();
			plot.drawAxis();
			for (auto& sample : samples)
			{
				plot.circle(sample.first[0], sample.first[1], 0.3, BLACK);
				plot.circle(sample.first[0], sample.first[1], 0.2, sample.second[0] > 0 ? RED : BLUE);
			}
			display.apply(plot);
			display.refresh();
			lastRefresh = chrono::steady_clock::now();
		}
	}

	cerr << "Initial loss: " << initloss << endl;
	cerr << "Final loss: " << calcLoss(net, samples) << endl;

	plot.gradient([&](float x, float y){return net.forwardPropagation({x,y})[0];}, 0.4, BLUE, RED);
	plot.drawGrid();
	plot.drawAxis();
	for (auto& sample : samples)
	{
		plot.circle(sample.first[0], sample.first[1], 0.3, BLACK);
		plot.circle(sample.first[0], sample.first[1], 0.2, sample.second[0] > 0 ? RED : BLUE);
	}
	display.apply(plot);
	display.refresh();
	if (keepRunning) getchar();

	return 0;
}
