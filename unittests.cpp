#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include "neuronaut.hpp"
#include <map>

using namespace std;
using namespace neuronaut;

// **** Aux functions ****

bool compare(float fval1, float fval2, unsigned int precision = 10)
{
	long long multiplier = pow(10,precision);
	long long ival1 = round( fval1 * multiplier);
	long long ival2 = round( fval2 * multiplier);
	return ival1 == ival2;
}


// **** TESTS ****

TEST_CASE("calculateNeuronsAmount")
{
	CHECK( calculateNeuronsAmount({})             == 0  );
	CHECK( calculateNeuronsAmount({1,1})          == 2  );
	CHECK( calculateNeuronsAmount({0,1})          == 1  );
	CHECK( calculateNeuronsAmount({1,0})          == 1  );
	CHECK( calculateNeuronsAmount({2,0})          == 2  );
	CHECK( calculateNeuronsAmount({0,2})          == 2  );
	CHECK( calculateNeuronsAmount({1,3,1})        == 5  );
	CHECK( calculateNeuronsAmount({1,3,3,1})      == 8  );
	CHECK( calculateNeuronsAmount({3,10,10,10,2}) == 35 );
	CHECK( calculateNeuronsAmount({3,10,16,3,2})  == 34 );
}

TEST_CASE("calculateConnectionsAmount")
{
	CHECK( calculateConnectionsAmount({})             == 0   );
	CHECK( calculateConnectionsAmount({1,1})          == 1   );
	CHECK( calculateConnectionsAmount({0,1})          == 0   );
	CHECK( calculateConnectionsAmount({1,0})          == 0   );
	CHECK( calculateConnectionsAmount({2,0})          == 0   );
	CHECK( calculateConnectionsAmount({0,2})          == 0   );
	CHECK( calculateConnectionsAmount({1,3,1})        == 6   );
	CHECK( calculateConnectionsAmount({1,3,3,1})      == 15  );
	CHECK( calculateConnectionsAmount({3,10,10,10,2}) == 250 );
	CHECK( calculateConnectionsAmount({3,2})          == 6   );
	CHECK( calculateConnectionsAmount({2,3})          == 6   );
	CHECK( calculateConnectionsAmount({3,10,16,3,2})  == 244 );
}


TEST_CASE("Given a network without hidden layers, the network is constructed correctly")
{
	SECTION("single input, single output")
	{
		Network net = buildNetwork({1,1}, linear);
		REQUIRE( net.layers.size()     == 2 );
		REQUIRE( net.layers[0].size()  == 1 );
		REQUIRE( net.layers[1].size()  == 1 );

		REQUIRE( net.layers[0][0].weights.size() == 1 );
		CHECK( net.layers[0][0].bias == 0 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );

		REQUIRE( net.layers[1][0].weights.size() == 0 );
		CHECK( net.layers[1][0].bias == 0 );
		CHECK( net.layers[1][0].output == 0 );
	}
	SECTION("three inputs, one output")
	{
		Network net = buildNetwork({3,1}, linear);
		REQUIRE( net.layers.size() == 2 );
		REQUIRE( net.layers[0].size()  == 3 );
		REQUIRE( net.layers[1].size() == 1 );

		REQUIRE( net.layers[0][0].weights.size() == 1 );
		CHECK( net.layers[0][0].bias == 0 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );

		REQUIRE( net.layers[0][1].weights.size() == 1 );
		CHECK( net.layers[0][1].bias == 0 );
		CHECK( net.layers[0][1].output == 0 );
		CHECK( net.layers[0][1].weights[0] == 1 );

		REQUIRE( net.layers[0][2].weights.size() == 1 );
		CHECK( net.layers[0][2].bias == 0 );
		CHECK( net.layers[0][2].output == 0 );
		CHECK( net.layers[0][2].weights[0] == 1 );

		REQUIRE( net.layers[1][0].weights.size() == 0 );
		CHECK( net.layers[1][0].bias == 0 );
		CHECK( net.layers[1][0].output == 0 );
	}
	SECTION("three inputs, two outputs")
	{
		Network net = buildNetwork({3,2}, linear);
		REQUIRE( net.layers.size() == 2 );
		REQUIRE( net.layers[0].size() == 3 );
		REQUIRE( net.layers[1].size() == 2 );

		REQUIRE( net.layers[0][0].weights.size() == 2 );
		CHECK( net.layers[0][0].bias == 0 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );
		CHECK( net.layers[0][0].weights[1] == 1 );

		REQUIRE( net.layers[0][1].weights.size() == 2 );
		CHECK( net.layers[0][1].bias == 0 );
		CHECK( net.layers[0][1].output == 0 );
		CHECK( net.layers[0][1].weights[0] == 1 );
		CHECK( net.layers[0][1].weights[1] == 1 );

		REQUIRE( net.layers[0][2].weights.size() == 2 );
		CHECK( net.layers[0][2].bias == 0 );
		CHECK( net.layers[0][2].output == 0 );
		CHECK( net.layers[0][2].weights[0] == 1 );
		CHECK( net.layers[0][2].weights[1] == 1 );


		REQUIRE( net.layers[1][0].weights.size() == 0 );
		CHECK( net.layers[1][0].bias == 0 );
		CHECK( net.layers[1][0].output == 0 );

		REQUIRE( net.layers[1][1].weights.size() == 0 );
		CHECK( net.layers[1][1].bias == 0 );
		CHECK( net.layers[1][1].output == 0 );
	}
	SECTION("three inputs, two outputs, biases and weights")
	{
		vector<float> biases = {1,2,3,4,5};
		vector<float> weights = {1,2,3,4,5,6};
		Network net = buildNetwork({3,2}, linear, biases, weights);
		REQUIRE( net.layers.size() == 2 );
		REQUIRE( net.layers[0].size() == 3 );
		REQUIRE( net.layers[1].size() == 2 );

		REQUIRE( net.layers[0][0].weights.size() == 2 );
		CHECK( net.layers[0][0].bias == 1 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );
		CHECK( net.layers[0][0].weights[1] == 2 );

		REQUIRE( net.layers[0][1].weights.size() == 2 );
		CHECK( net.layers[0][1].bias == 2 );
		CHECK( net.layers[0][1].output == 0 );
		CHECK( net.layers[0][1].weights[0] == 3 );
		CHECK( net.layers[0][1].weights[1] == 4 );

		REQUIRE( net.layers[0][2].weights.size() == 2 );
		CHECK( net.layers[0][2].bias == 3 );
		CHECK( net.layers[0][2].output == 0 );
		CHECK( net.layers[0][2].weights[0] == 5 );
		CHECK( net.layers[0][2].weights[1] == 6 );

		REQUIRE( net.layers[1][0].weights.size() == 0 );
		CHECK( net.layers[1][0].bias == 4 );
		CHECK( net.layers[1][0].output == 0 );

		REQUIRE( net.layers[1][1].weights.size() == 0 );
		CHECK( net.layers[1][1].bias == 5 );
		CHECK( net.layers[1][1].output == 0 );
	}
}

TEST_CASE("Given a network with a single hidden layers, the network is constructed correctly")
{
	SECTION("single input, single output, single neuron per layer")
	{
		Network net = buildNetwork({1,1,1}, linear);
		REQUIRE( net.layers.size() == 3 );
		REQUIRE( net.layers[0].size() == 1 );
		REQUIRE( net.layers[1].size() == 1 );
		REQUIRE( net.layers[2].size() == 1 );

		REQUIRE( net.layers[0][0].weights.size() == 1 );
		CHECK( net.layers[0][0].bias == 0 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );

		REQUIRE( net.layers[1][0].weights.size() == 1 );
		CHECK( net.layers[1][0].bias == 0 );
		CHECK( net.layers[1][0].output == 0 );
		CHECK( net.layers[1][0].weights[0] == 1 );

		REQUIRE( net.layers[2][0].weights.size() == 0 );
		CHECK( net.layers[2][0].bias == 0 );
		CHECK( net.layers[2][0].output == 0 );
	}
	SECTION("two inputs, two outputs, two neuron per layer")
	{
		Network net = buildNetwork({2,2,2}, linear);
		REQUIRE( net.layers.size() == 3 );
		REQUIRE( net.layers[0].size() == 2 );
		REQUIRE( net.layers[1].size() == 2 );
		REQUIRE( net.layers[2].size() == 2 );

		REQUIRE( net.layers[0][0].weights.size() == 2 );
		CHECK( net.layers[0][0].bias == 0 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );
		CHECK( net.layers[0][0].weights[1] == 1 );

		REQUIRE( net.layers[0][1].weights.size() == 2 );
		CHECK( net.layers[0][1].bias == 0 );
		CHECK( net.layers[0][1].output == 0 );
		CHECK( net.layers[0][1].weights[0] == 1 );
		CHECK( net.layers[0][1].weights[1] == 1 );

		REQUIRE( net.layers[1][0].weights.size() == 2 );
		CHECK( net.layers[1][0].bias == 0 );
		CHECK( net.layers[1][0].output == 0 );
		CHECK( net.layers[1][0].weights[0] == 1 );
		CHECK( net.layers[1][0].weights[1] == 1 );

		REQUIRE( net.layers[1][1].weights.size() == 2 );
		CHECK( net.layers[1][1].bias == 0 );
		CHECK( net.layers[1][1].output == 0 );
		CHECK( net.layers[1][1].weights[0] == 1 );
		CHECK( net.layers[1][1].weights[1] == 1 );

		REQUIRE( net.layers[2][0].weights.size() == 0 );
		CHECK( net.layers[2][0].bias == 0 );
		CHECK( net.layers[2][0].output == 0 );

		REQUIRE( net.layers[2][1].weights.size() == 0 );
		CHECK( net.layers[2][1].bias == 0 );
		CHECK( net.layers[2][1].output == 0 );
	}
}
TEST_CASE("Given a network with different hidden layers, the network is constructed correctly")
{
	SECTION("single input, single output, three hidden layers, single neuron per layer")
	{
		Network net = buildNetwork({1, 1,1,1, 1}, linear);
		REQUIRE( net.layers.size() == 5 );
		REQUIRE( net.layers[0].size() == 1 );
		REQUIRE( net.layers[1].size() == 1 );
		REQUIRE( net.layers[2].size() == 1 );
		REQUIRE( net.layers[3].size() == 1 );
		REQUIRE( net.layers[4].size() == 1 );

		REQUIRE( net.layers[0][0].weights.size() == 1 );
		CHECK( net.layers[0][0].bias == 0 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );

		REQUIRE( net.layers[1][0].weights.size() == 1 );
		CHECK( net.layers[1][0].bias == 0 );
		CHECK( net.layers[1][0].output == 0 );
		CHECK( net.layers[1][0].weights[0] == 1 );

		REQUIRE( net.layers[2][0].weights.size() == 1 );
		CHECK( net.layers[2][0].bias == 0 );
		CHECK( net.layers[2][0].output == 0 );
		CHECK( net.layers[2][0].weights[0] == 1 );

		REQUIRE( net.layers[3][0].weights.size() == 1 );
		CHECK( net.layers[3][0].bias == 0 );
		CHECK( net.layers[3][0].output == 0 );
		CHECK( net.layers[3][0].weights[0] == 1 );

		REQUIRE( net.layers[4][0].weights.size() == 0 );
		CHECK( net.layers[4][0].bias == 0 );
		CHECK( net.layers[4][0].output == 0 ); 
	}
	SECTION("three inputs, three outputs, three hidden layers, three neurons per layer")
	{
		Network net = buildNetwork({3, 3,3,3, 3}, linear);
		REQUIRE( net.layers.size() == 5 );
		REQUIRE( net.layers[0].size() == 3 );
		REQUIRE( net.layers[1].size() == 3 );
		REQUIRE( net.layers[2].size() == 3 );
		REQUIRE( net.layers[3].size() == 3 );
		REQUIRE( net.layers[4].size() == 3 );

		REQUIRE( net.layers[0][0].weights.size() == 3 );
		CHECK( net.layers[0][0].bias == 0 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );
		CHECK( net.layers[0][0].weights[1] == 1 );
		CHECK( net.layers[0][0].weights[2] == 1 );

		REQUIRE( net.layers[0][1].weights.size() == 3 );
		CHECK( net.layers[0][1].bias == 0 );
		CHECK( net.layers[0][1].output == 0 );
		CHECK( net.layers[0][1].weights[0] == 1 );
		CHECK( net.layers[0][1].weights[1] == 1 );
		CHECK( net.layers[0][1].weights[2] == 1 );

		REQUIRE( net.layers[0][2].weights.size() == 3 );
		CHECK( net.layers[0][2].bias == 0 );
		CHECK( net.layers[0][2].output == 0 );
		CHECK( net.layers[0][2].weights[0] == 1 );
		CHECK( net.layers[0][2].weights[1] == 1 );
		CHECK( net.layers[0][2].weights[2] == 1 );

		REQUIRE( net.layers[1][0].weights.size() == 3 );
		CHECK( net.layers[1][0].bias == 0 );
		CHECK( net.layers[1][0].output == 0 );
		CHECK( net.layers[1][0].weights[0] == 1 );
		CHECK( net.layers[1][0].weights[1] == 1 );
		CHECK( net.layers[1][0].weights[2] == 1 );

		REQUIRE( net.layers[1][1].weights.size() == 3 );
		CHECK( net.layers[1][1].bias == 0 );
		CHECK( net.layers[1][1].output == 0 );
		CHECK( net.layers[1][1].weights[0] == 1 );
		CHECK( net.layers[1][1].weights[1] == 1 );
		CHECK( net.layers[1][1].weights[2] == 1 );

		REQUIRE( net.layers[1][2].weights.size() == 3 );
		CHECK( net.layers[1][2].bias == 0 );
		CHECK( net.layers[1][2].output == 0 );
		CHECK( net.layers[1][2].weights[0] == 1 );
		CHECK( net.layers[1][2].weights[1] == 1 );
		CHECK( net.layers[1][2].weights[2] == 1 );

		REQUIRE( net.layers[2][0].weights.size() == 3 );
		CHECK( net.layers[2][0].bias == 0 );
		CHECK( net.layers[2][0].output == 0 );
		CHECK( net.layers[2][0].weights[0] == 1 );
		CHECK( net.layers[2][0].weights[1] == 1 );
		CHECK( net.layers[2][0].weights[2] == 1 );

		REQUIRE( net.layers[2][1].weights.size() == 3 );
		CHECK( net.layers[2][1].bias == 0 );
		CHECK( net.layers[2][1].output == 0 );
		CHECK( net.layers[2][1].weights[0] == 1 );
		CHECK( net.layers[2][1].weights[1] == 1 );
		CHECK( net.layers[2][1].weights[2] == 1 );

		REQUIRE( net.layers[2][2].weights.size() == 3 );
		CHECK( net.layers[2][2].bias == 0 );
		CHECK( net.layers[2][2].output == 0 );
		CHECK( net.layers[2][2].weights[0] == 1 );
		CHECK( net.layers[2][2].weights[1] == 1 );
		CHECK( net.layers[2][2].weights[2] == 1 );

		REQUIRE( net.layers[3][0].weights.size() == 3 );
		CHECK( net.layers[3][0].bias == 0 );
		CHECK( net.layers[3][0].output == 0 );
		CHECK( net.layers[3][0].weights[0] == 1 );
		CHECK( net.layers[3][0].weights[1] == 1 );
		CHECK( net.layers[3][0].weights[2] == 1 );

		REQUIRE( net.layers[3][1].weights.size() == 3 );
		CHECK( net.layers[3][1].bias == 0 );
		CHECK( net.layers[3][1].output == 0 );
		CHECK( net.layers[3][1].weights[0] == 1 );
		CHECK( net.layers[3][1].weights[1] == 1 );
		CHECK( net.layers[3][1].weights[2] == 1 );

		REQUIRE( net.layers[3][2].weights.size() == 3 );
		CHECK( net.layers[3][2].bias == 0 );
		CHECK( net.layers[3][2].output == 0 );
		CHECK( net.layers[3][2].weights[0] == 1 );
		CHECK( net.layers[3][2].weights[1] == 1 );
		CHECK( net.layers[3][2].weights[2] == 1 );

		REQUIRE( net.layers[4][0].weights.size() == 0 );
		CHECK( net.layers[4][0].bias == 0 );
		CHECK( net.layers[4][0].output == 0 );
		REQUIRE( net.layers[4][1].weights.size() == 0 );
		CHECK( net.layers[4][1].bias == 0 );
		CHECK( net.layers[4][1].output == 0 );
		REQUIRE( net.layers[4][2].weights.size() == 0 );
		CHECK( net.layers[4][2].bias == 0 );
		CHECK( net.layers[4][2].output == 0 );
	}
	SECTION("three inputs, three outputs, three hidden layers, three neurons per layer, biases and weights")
	{
		vector<float> biases = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		vector<float> weights = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36};
		Network net = buildNetwork({3, 3,3,3, 3}, linear, biases, weights);
		REQUIRE( net.layers.size() == 5 );
		REQUIRE( net.layers[0].size() == 3 );
		REQUIRE( net.layers[1].size() == 3 );
		REQUIRE( net.layers[2].size() == 3 );
		REQUIRE( net.layers[3].size() == 3 );
		REQUIRE( net.layers[4].size() == 3 );

		REQUIRE( net.layers[0][0].weights.size() == 3 );
		CHECK( net.layers[0][0].bias == 1 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );
		CHECK( net.layers[0][0].weights[1] == 2 );
		CHECK( net.layers[0][0].weights[2] == 3 );

		REQUIRE( net.layers[0][1].weights.size() == 3 );
		CHECK( net.layers[0][1].bias == 2 );
		CHECK( net.layers[0][1].output == 0 );
		CHECK( net.layers[0][1].weights[0] == 4 );
		CHECK( net.layers[0][1].weights[1] == 5 );
		CHECK( net.layers[0][1].weights[2] == 6 );

		REQUIRE( net.layers[0][2].weights.size() == 3 );
		CHECK( net.layers[0][2].bias == 3 );
		CHECK( net.layers[0][2].output == 0 );
		CHECK( net.layers[0][2].weights[0] == 7 );
		CHECK( net.layers[0][2].weights[1] == 8 );
		CHECK( net.layers[0][2].weights[2] == 9 );

		REQUIRE( net.layers[1][0].weights.size() == 3 );
		CHECK( net.layers[1][0].bias == 4 );
		CHECK( net.layers[1][0].output == 0 );
		CHECK( net.layers[1][0].weights[0] == 10 );
		CHECK( net.layers[1][0].weights[1] == 11 );
		CHECK( net.layers[1][0].weights[2] == 12 );

		REQUIRE( net.layers[1][1].weights.size() == 3 );
		CHECK( net.layers[1][1].bias == 5 );
		CHECK( net.layers[1][1].output == 0 );
		CHECK( net.layers[1][1].weights[0] == 13 );
		CHECK( net.layers[1][1].weights[1] == 14 );
		CHECK( net.layers[1][1].weights[2] == 15 );

		REQUIRE( net.layers[1][2].weights.size() == 3 );
		CHECK( net.layers[1][2].bias == 6 );
		CHECK( net.layers[1][2].output == 0 );
		CHECK( net.layers[1][2].weights[0] == 16 );
		CHECK( net.layers[1][2].weights[1] == 17 );
		CHECK( net.layers[1][2].weights[2] == 18 );

		REQUIRE( net.layers[2][0].weights.size() == 3 );
		CHECK( net.layers[2][0].bias == 7 );
		CHECK( net.layers[2][0].output == 0 );
		CHECK( net.layers[2][0].weights[0] == 19 );
		CHECK( net.layers[2][0].weights[1] == 20 );
		CHECK( net.layers[2][0].weights[2] == 21 );

		REQUIRE( net.layers[2][1].weights.size() == 3 );
		CHECK( net.layers[2][1].bias == 8 );
		CHECK( net.layers[2][1].output == 0 );
		CHECK( net.layers[2][1].weights[0] == 22 );
		CHECK( net.layers[2][1].weights[1] == 23 );
		CHECK( net.layers[2][1].weights[2] == 24 );

		REQUIRE( net.layers[2][2].weights.size() == 3 );
		CHECK( net.layers[2][2].bias == 9 );
		CHECK( net.layers[2][2].output == 0 );
		CHECK( net.layers[2][2].weights[0] == 25 );
		CHECK( net.layers[2][2].weights[1] == 26 );
		CHECK( net.layers[2][2].weights[2] == 27 );

		REQUIRE( net.layers[3][0].weights.size() == 3 );
		CHECK( net.layers[3][0].bias == 10 );
		CHECK( net.layers[3][0].output == 0 );
		CHECK( net.layers[3][0].weights[0] == 28 );
		CHECK( net.layers[3][0].weights[1] == 29 );
		CHECK( net.layers[3][0].weights[2] == 30 );

		REQUIRE( net.layers[3][1].weights.size() == 3 );
		CHECK( net.layers[3][1].bias == 11 );
		CHECK( net.layers[3][1].output == 0 );
		CHECK( net.layers[3][1].weights[0] == 31 );
		CHECK( net.layers[3][1].weights[1] == 32 );
		CHECK( net.layers[3][1].weights[2] == 33 );

		REQUIRE( net.layers[3][2].weights.size() == 3 );
		CHECK( net.layers[3][2].bias == 12 );
		CHECK( net.layers[3][2].output == 0 );
		CHECK( net.layers[3][2].weights[0] == 34 );
		CHECK( net.layers[3][2].weights[1] == 35 );
		CHECK( net.layers[3][2].weights[2] == 36 );

		REQUIRE( net.layers[4][0].weights.size() == 0 );
		CHECK( net.layers[4][0].bias == 13 );
		CHECK( net.layers[4][0].output == 0 );
		CHECK( net.layers[4][1].weights.size() == 0 );
		CHECK( net.layers[4][1].bias == 14 );
		CHECK( net.layers[4][1].output == 0 );
		CHECK( net.layers[4][2].weights.size() == 0 );
		CHECK( net.layers[4][2].bias == 15 );
		CHECK( net.layers[4][2].output == 0 );
	}
	SECTION("three inputs, three outputs, three hidden layers, different neurons per layer, biases and weights")
	{
		vector<float> biases = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		vector<float> weights = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35};
		Network net = buildNetwork({3, 3,4,2, 3}, linear, biases, weights);
		REQUIRE( net.layers.size() == 5 );
		REQUIRE( net.layers[0].size() == 3 );
		REQUIRE( net.layers[1].size() == 3 );
		REQUIRE( net.layers[2].size() == 4 );
		REQUIRE( net.layers[3].size() == 2 );
		REQUIRE( net.layers[4].size() == 3 );

		REQUIRE( net.layers[0][0].weights.size() == 3 );
		CHECK( net.layers[0][0].bias == 1 );
		CHECK( net.layers[0][0].output == 0 );
		CHECK( net.layers[0][0].weights[0] == 1 );
		CHECK( net.layers[0][0].weights[1] == 2 );
		CHECK( net.layers[0][0].weights[2] == 3 );

		REQUIRE( net.layers[0][1].weights.size() == 3 );
		CHECK( net.layers[0][1].bias == 2 );
		CHECK( net.layers[0][1].output == 0 );
		CHECK( net.layers[0][1].weights[0] == 4 );
		CHECK( net.layers[0][1].weights[1] == 5 );
		CHECK( net.layers[0][1].weights[2] == 6 );

		REQUIRE( net.layers[0][2].weights.size() == 3 );
		CHECK( net.layers[0][2].bias == 3 );
		CHECK( net.layers[0][2].output == 0 );
		CHECK( net.layers[0][2].weights[0] == 7 );
		CHECK( net.layers[0][2].weights[1] == 8 );
		CHECK( net.layers[0][2].weights[2] == 9 );

		REQUIRE( net.layers[1][0].weights.size() == 4 );
		CHECK( net.layers[1][0].bias == 4 );
		CHECK( net.layers[1][0].output == 0 );
		CHECK( net.layers[1][0].weights[0] == 10 );
		CHECK( net.layers[1][0].weights[1] == 11 );
		CHECK( net.layers[1][0].weights[2] == 12 );
		CHECK( net.layers[1][0].weights[3] == 13 );

		REQUIRE( net.layers[1][1].weights.size() == 4 );
		CHECK( net.layers[1][1].bias == 5 );
		CHECK( net.layers[1][1].output == 0 );
		CHECK( net.layers[1][1].weights[0] == 14 );
		CHECK( net.layers[1][1].weights[1] == 15 );
		CHECK( net.layers[1][1].weights[2] == 16 );
		CHECK( net.layers[1][1].weights[3] == 17 );

		REQUIRE( net.layers[1][2].weights.size() == 4 );
		CHECK( net.layers[1][2].bias == 6 );
		CHECK( net.layers[1][2].output == 0 );
		CHECK( net.layers[1][2].weights[0] == 18 );
		CHECK( net.layers[1][2].weights[1] == 19 );
		CHECK( net.layers[1][2].weights[2] == 20 );
		CHECK( net.layers[1][2].weights[3] == 21 );

		REQUIRE( net.layers[2][0].weights.size() == 2 );
		CHECK( net.layers[2][0].bias == 7 );
		CHECK( net.layers[2][0].output == 0 );
		CHECK( net.layers[2][0].weights[0] == 22 );
		CHECK( net.layers[2][0].weights[1] == 23 );

		REQUIRE( net.layers[2][1].weights.size() == 2 );
		CHECK( net.layers[2][1].bias == 8 );
		CHECK( net.layers[2][1].output == 0 );
		CHECK( net.layers[2][1].weights[0] == 24 );
		CHECK( net.layers[2][1].weights[1] == 25 );

		REQUIRE( net.layers[2][2].weights.size() == 2 );
		CHECK( net.layers[2][2].bias == 9 );
		CHECK( net.layers[2][2].output == 0 );
		CHECK( net.layers[2][2].weights[0] == 26 );
		CHECK( net.layers[2][2].weights[1] == 27 );

		REQUIRE( net.layers[2][3].weights.size() == 2 );
		CHECK( net.layers[2][3].bias == 10 );
		CHECK( net.layers[2][3].output == 0 );
		CHECK( net.layers[2][3].weights[0] == 28 );
		CHECK( net.layers[2][3].weights[1] == 29 );

		REQUIRE( net.layers[3][0].weights.size() == 3 );
		CHECK( net.layers[3][0].bias == 11 );
		CHECK( net.layers[3][0].output == 0 );
		CHECK( net.layers[3][0].weights[0] == 30 );
		CHECK( net.layers[3][0].weights[1] == 31 );
		CHECK( net.layers[3][0].weights[2] == 32 );

		REQUIRE( net.layers[3][1].weights.size() == 3 );
		CHECK( net.layers[3][1].bias == 12 );
		CHECK( net.layers[3][1].output == 0 );
		CHECK( net.layers[3][1].weights[0] == 33 );
		CHECK( net.layers[3][1].weights[1] == 34 );
		CHECK( net.layers[3][1].weights[2] == 35 );

		REQUIRE( net.layers[4][0].weights.size() == 0 );
		CHECK( net.layers[4][0].bias == 13 );
		CHECK( net.layers[4][0].output == 0 );
		REQUIRE( net.layers[4][1].weights.size() == 0 );
		CHECK( net.layers[4][1].bias == 14 );
		CHECK( net.layers[4][1].output == 0 );
		REQUIRE( net.layers[4][2].weights.size() == 0 );
		CHECK( net.layers[4][2].bias == 15 );
		CHECK( net.layers[4][2].output == 0 );
	}
}

TEST_CASE("Copy network")
{
	vector<float> biases = {1,2,3,  4,5,6,7,8,  9,10,11,12,  13,14,15,  16,17};
	vector<float> weights = {
		1,2,3,4,5, 6,7,8,9,10, 11,12,13,14,15,
		16,17,18,19,  20,21,22,23,  24,25,26,27,  28,29,30,31,  32,33,34,35,
		36,37,38,  39,40,41,  42,43,44,  45,46,47,
		48,49, 50,51, 52,53,
	};
	Network orig = buildNetwork({3, 5,4,3, 2}, relu, biases, weights);
	Network dest = orig;

	REQUIRE( dest.layers.size() == 5 );
	REQUIRE( dest.layers[0].size() == 3 );
	REQUIRE( dest.layers[1].size() == 5 );
	REQUIRE( dest.layers[2].size() == 4 );
	REQUIRE( dest.layers[3].size() == 3 );
	REQUIRE( dest.layers[4].size() == 2 );

	CHECK( dest.layers[0][0].bias == 1);
	REQUIRE( dest.layers[0][0].weights.size() == 5);
	CHECK( dest.layers[0][0].weights[0] == 1);
	CHECK( dest.layers[0][0].weights[1] == 2);
	CHECK( dest.layers[0][0].weights[2] == 3);
	CHECK( dest.layers[0][0].weights[3] == 4);
	CHECK( dest.layers[0][0].weights[4] == 5);

	CHECK( dest.layers[0][1].bias == 2);
	REQUIRE( dest.layers[0][1].weights.size() == 5);
	CHECK( dest.layers[0][1].weights[0] == 6);
	CHECK( dest.layers[0][1].weights[1] == 7);
	CHECK( dest.layers[0][1].weights[2] == 8);
	CHECK( dest.layers[0][1].weights[3] == 9);
	CHECK( dest.layers[0][1].weights[4] == 10);

	CHECK( dest.layers[0][2].bias == 3);
	REQUIRE( dest.layers[0][2].weights.size() == 5);
	CHECK( dest.layers[0][2].weights[0] == 11);
	CHECK( dest.layers[0][2].weights[1] == 12);
	CHECK( dest.layers[0][2].weights[2] == 13);
	CHECK( dest.layers[0][2].weights[3] == 14);
	CHECK( dest.layers[0][2].weights[4] == 15);

	CHECK( dest.layers[1][0].bias == 4);
	REQUIRE( dest.layers[1][0].weights.size() == 4);
	CHECK( dest.layers[1][0].weights[0] == 16);
	CHECK( dest.layers[1][0].weights[1] == 17);
	CHECK( dest.layers[1][0].weights[2] == 18);
	CHECK( dest.layers[1][0].weights[3] == 19);

	CHECK( dest.layers[1][1].bias == 5);
	REQUIRE( dest.layers[1][1].weights.size() == 4);
	CHECK( dest.layers[1][1].weights[0] == 20);
	CHECK( dest.layers[1][1].weights[1] == 21);
	CHECK( dest.layers[1][1].weights[2] == 22);
	CHECK( dest.layers[1][1].weights[3] == 23);

	CHECK( dest.layers[1][2].bias == 6);
	REQUIRE( dest.layers[1][2].weights.size() == 4);
	CHECK( dest.layers[1][2].weights[0] == 24);
	CHECK( dest.layers[1][2].weights[1] == 25);
	CHECK( dest.layers[1][2].weights[2] == 26);
	CHECK( dest.layers[1][2].weights[3] == 27);

	CHECK( dest.layers[1][3].bias == 7);
	REQUIRE( dest.layers[1][3].weights.size() == 4);
	CHECK( dest.layers[1][3].weights[0] == 28);
	CHECK( dest.layers[1][3].weights[1] == 29);
	CHECK( dest.layers[1][3].weights[2] == 30);
	CHECK( dest.layers[1][3].weights[3] == 31);

	CHECK( dest.layers[1][4].bias == 8);
	REQUIRE( dest.layers[1][4].weights.size() == 4);
	CHECK( dest.layers[1][4].weights[0] == 32);
	CHECK( dest.layers[1][4].weights[1] == 33);
	CHECK( dest.layers[1][4].weights[2] == 34);
	CHECK( dest.layers[1][4].weights[3] == 35);

	CHECK( dest.layers[2][0].bias == 9);
	REQUIRE( dest.layers[2][0].weights.size() == 3);
	CHECK( dest.layers[2][0].weights[0] == 36);
	CHECK( dest.layers[2][0].weights[1] == 37);
	CHECK( dest.layers[2][0].weights[2] == 38);

	CHECK( dest.layers[2][1].bias == 10);
	REQUIRE( dest.layers[2][1].weights.size() == 3);
	CHECK( dest.layers[2][1].weights[0] == 39);
	CHECK( dest.layers[2][1].weights[1] == 40);
	CHECK( dest.layers[2][1].weights[2] == 41);

	CHECK( dest.layers[2][2].bias == 11);
	REQUIRE( dest.layers[2][2].weights.size() == 3);
	CHECK( dest.layers[2][2].weights[0] == 42);
	CHECK( dest.layers[2][2].weights[1] == 43);
	CHECK( dest.layers[2][2].weights[2] == 44);

	CHECK( dest.layers[2][3].bias == 12);
	REQUIRE( dest.layers[2][3].weights.size() == 3);
	CHECK( dest.layers[2][3].weights[0] == 45);
	CHECK( dest.layers[2][3].weights[1] == 46);
	CHECK( dest.layers[2][3].weights[2] == 47);

	CHECK( dest.layers[3][0].bias == 13);
	REQUIRE( dest.layers[3][0].weights.size() == 2);
	CHECK( dest.layers[3][0].weights[0] == 48);
	CHECK( dest.layers[3][0].weights[1] == 49);

	CHECK( dest.layers[3][1].bias == 14);
	REQUIRE( dest.layers[3][1].weights.size() == 2);
	CHECK( dest.layers[3][1].weights[0] == 50);
	CHECK( dest.layers[3][1].weights[1] == 51);

	CHECK( dest.layers[3][2].bias == 15);
	REQUIRE( dest.layers[3][2].weights.size() == 2);
	CHECK( dest.layers[3][2].weights[0] == 52);
	CHECK( dest.layers[3][2].weights[1] == 53);

	CHECK( dest.layers[4][0].bias == 16 );
	CHECK( dest.layers[4][0].weights.size() == 0);

	CHECK( dest.layers[4][1].bias == 17 );
	CHECK( dest.layers[4][1].weights.size() == 0);
}


TEST_CASE("forwardPropagation")
{
	SECTION("single input and output, no hidden layer, function linear")
	{
		Network net = buildNetwork({1,1}, linear);
		CHECK( net.forwardPropagation({1.4f}) == vector<float>{1.4f} );
	}
	SECTION("single input and output, some hidden layers, function linear")
	{
		Network net = buildNetwork({1, 1,1,1, 1}, linear);
		CHECK( net.forwardPropagation({1.4}) == vector<float>{1.4f} );
	}
	SECTION("single input and output, some hidden layers, function linear")
	{
		Network net = buildNetwork({1, 3,3,3, 1}, linear);
		CHECK( net.forwardPropagation({1.0f}) == vector<float>{27.0f} );
	}
		
	SECTION("")
	{
		Network net = buildNetwork({1,1}, linear);

		CHECK( net.forwardPropagation({1})   == vector<float>{ 1.0f} );
		CHECK( net.forwardPropagation({0})   == vector<float>{ 0.0f} );
		CHECK( net.forwardPropagation({0.3}) == vector<float>{ 0.3f} );
		CHECK( net.forwardPropagation({-4})  == vector<float>{-4.0f} );
	}
}
TEST_CASE("given an vector of floats, setBias sets the biases for all the neurons in the network")
{
	Network net = buildNetwork({3, 3,3,3, 2}, linear);

	net.setBias({1,2,3,4,5,6,7,8,9,10,11,12,13,14});

	CHECK(net.layers[0][0].bias == 1);
	CHECK(net.layers[0][1].bias == 2);
	CHECK(net.layers[0][2].bias == 3);

	CHECK(net.layers[1][0].bias == 4);
	CHECK(net.layers[1][1].bias == 5);
	CHECK(net.layers[1][2].bias == 6);

	CHECK(net.layers[2][0].bias == 7);
	CHECK(net.layers[2][1].bias == 8);
	CHECK(net.layers[2][2].bias == 9);

	CHECK(net.layers[3][0].bias == 10);
	CHECK(net.layers[3][1].bias == 11);
	CHECK(net.layers[3][2].bias == 12);

	CHECK(net.layers[4][0].bias == 13);
	CHECK(net.layers[4][1].bias == 14);
}
TEST_CASE("given a vector of floats, setWeights sets the weights for all the connections in the network")
{
	SECTION("When the network doesn' have hidden layers")
	{
		Network net = buildNetwork({3,2}, linear);

		net.setWeights({1,2,3,4,5,6});

		CHECK(net.layers[0][0].weights[0] == 1);
		CHECK(net.layers[0][0].weights[1] == 2);

		CHECK(net.layers[0][1].weights[0] == 3);
		CHECK(net.layers[0][1].weights[1] == 4);

		CHECK(net.layers[0][2].weights[0] == 5);
		CHECK(net.layers[0][2].weights[1] == 6);
	}
	SECTION("when the network has hidden layers")
	{
		Network net = buildNetwork({3, 3,3,3, 2}, linear);

		net.setWeights({1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33});

		CHECK(net.layers[0][0].weights[0] == 1);
		CHECK(net.layers[0][0].weights[1] == 2);
		CHECK(net.layers[0][0].weights[2] == 3);

		CHECK(net.layers[0][1].weights[0] == 4);
		CHECK(net.layers[0][1].weights[1] == 5);
		CHECK(net.layers[0][1].weights[2] == 6);

		CHECK(net.layers[0][2].weights[0] == 7);
		CHECK(net.layers[0][2].weights[1] == 8);
		CHECK(net.layers[0][2].weights[2] == 9);

		CHECK(net.layers[1][0].weights[0] == 10);
		CHECK(net.layers[1][0].weights[1] == 11);
		CHECK(net.layers[1][0].weights[2] == 12);

		CHECK(net.layers[1][1].weights[0] == 13);
		CHECK(net.layers[1][1].weights[1] == 14);
		CHECK(net.layers[1][1].weights[2] == 15);

		CHECK(net.layers[1][2].weights[0] == 16);
		CHECK(net.layers[1][2].weights[1] == 17);
		CHECK(net.layers[1][2].weights[2] == 18);

		CHECK(net.layers[2][0].weights[0] == 19);
		CHECK(net.layers[2][0].weights[1] == 20);
		CHECK(net.layers[2][0].weights[2] == 21);

		CHECK(net.layers[2][1].weights[0] == 22);
		CHECK(net.layers[2][1].weights[1] == 23);
		CHECK(net.layers[2][1].weights[2] == 24);

		CHECK(net.layers[2][2].weights[0] == 25);
		CHECK(net.layers[2][2].weights[1] == 26);
		CHECK(net.layers[2][2].weights[2] == 27);

		CHECK(net.layers[3][0].weights[0] == 28);
		CHECK(net.layers[3][0].weights[1] == 29);

		CHECK(net.layers[3][1].weights[0] == 30);
		CHECK(net.layers[3][1].weights[1] == 31);

		CHECK(net.layers[3][2].weights[0] == 32);
		CHECK(net.layers[3][2].weights[1] == 33);
	}
}
TEST_CASE("getBias returns a vector with the biases from all the neurons in a network")
{
	Network net = buildNetwork({3, 3,3,3, 2}, linear);
	net.setBias({1,2,3,4,5,6,7,8,9,10,11,12,13,14});

	vector<float> result = net.getBias();

	CHECK(result == vector<float>{1,2,3,4,5,6,7,8,9,10,11,12,13,14});
}
TEST_CASE("getWeights returns a vector with the weights from all connections in a network")
{
	SECTION("When the network doesn' have hidden layers")
	{
		Network net = buildNetwork({3,2}, linear);
		net.setWeights({1,2,3,4,5,6});

		vector<float> result = net.getWeights();

		CHECK(result == vector<float>{1,2,3,4,5,6});
	}
	SECTION("when the network has hidden layers")
	{
		Network net = buildNetwork({3, 3,3,3, 2}, linear);
		net.setWeights({1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33});

		vector<float> result = net.getWeights();

		CHECK(result == vector<float>{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33});
	}
}

TEST_CASE("Linear function")
{
	map<float,float> expectations = {
		{-2, -2},
		{-1, -1},
		{-0.5, -0.5},
		{0, 0},
		{0.5, 0.5},
		{1, 1},
		{2, 2}
	};

	for (auto& i : expectations)
	{
		float given = i.first;
		float expected = i.second;
		float result = linear(given);

		INFO("given: " << given << ", expected: " << expected << ", result: " << result);
		CHECK( compare(result, expected, 6) );
	}
}

TEST_CASE("binaryStep function")
{
	map<float,float> expectations = {
		{-2, 0},
		{-1, 0},
		{-0.5, 0},
		{0, 1},
		{0.5, 1},
		{1, 1},
		{2, 1}
	};

	for (auto& i : expectations)
	{
		float given = i.first;
		float expected = i.second;
		float result = binaryStep(given);

		INFO("given: " << given << ", expected: " << expected << ", result: " << result);
		CHECK( compare(result, expected, 6) );
	}
}

TEST_CASE("signum function")
{
	map<float,float> expectations = {
		{-2, -1},
		{-1, -1},
		{-0.5, -1},
		{0, 0},
		{0.5, 1},
		{1, 1},
		{2, 1}
	};

	for (auto& i : expectations)
	{
		float given = i.first;
		float expected = i.second;
		float result = signum(given);

		INFO("given: " << given << ", expected: " << expected << ", result: " << result);
		CHECK( compare(result, expected, 6) );
	}
}

TEST_CASE("relu function")
{
	map<float,float> expectations = {
		{-2, 0},
		{-1, 0},
		{-0.5, 0},
		{0, 0},
		{0.5, 0.5},
		{1, 1},
		{2, 2}
	};

	for (auto& i : expectations)
	{
		float given = i.first;
		float expected = i.second;
		float result = relu(given);

		INFO("given: " << given << ", expected: " << expected << ", result: " << result);
		CHECK( compare(result, expected, 6) );
	}
}
TEST_CASE("sigmoid function")
{
	map<float,float> expectations = {
		{-2, 0.119202922},
		{-1, 0.2689414214},
		{-0.5, 0.3775406688},
		{0, 0.5},
		{0.5, 0.6224593312},
		{1, 0.7310585786},
		{2, 0.880797078}
	};

	for (auto& i : expectations)
	{
		float given = i.first;
		float expected = i.second;
		float result = sigmoid(given);

		INFO("given: " << given << ", expected: " << expected << ", result: " << result);
		CHECK( compare(result, expected, 6) );
	}
}
TEST_CASE("softplus function")
{
	map<float,float> expectations = {
		{-2, 0.126928011},
		{-1, 0.3132616875},
		{-0.5, 0.4740769842},
		{0, 0.6931471806},
		{0.5, 0.9740769842},
		{1, 1.313261688},
		{2, 2.126928011}
	};

	for (auto& i : expectations)
	{
		float given = i.first;
		float expected = i.second;
		float result = softplus(given);

		INFO("given: " << given << ", expected: " << expected << ", result: " << result);
		CHECK( compare(result, expected, 6) );
	}
}
TEST_CASE("tanh function")
{
	map<float,float> expectations = {
		{-2, -0.9640275801},
		{-1, -0.761594156},
		{-0.5, -0.4621171573},
		{0, 0},
		{0.5, 0.4621171573},
		{1, 0.761594156},
		{2, 0.9640275801}
	};

	for (auto& i : expectations)
	{
		float given = i.first;
		float expected = i.second;
		float result = neuronaut::tanh(given);

		INFO("given: " << given << ", expected: " << expected << ", result: " << result);
		CHECK( compare(result, expected, 6) );
	}
}

TEST_CASE("normalize")
{
	map<float,float> given = {
		{3.5, 7},
		{1, 8},
		{22, 9},
		{0.5, 33}
	};
	map< vector<float>, vector<float> > expected = {
		{{3.5}, {7}},
		{{1}, {8}},
		{{22}, {9}},
		{{0.5}, {33}}
	};

	auto result = normalize(given);

	CHECK( result == expected );
}

TEST_CASE("spread")
{
	map< vector<float>, vector<float> > given = {
		{{3.5}, {7}},
		{{1}, {8}},
		{{22}, {9}},
		{{0.5}, {33}},
		{{22,33,8}, {44}},
		{{5,2}, {99,7}},
		{{9}, {66,99,69}},
		{{}, {0,0}}
	};
	vector< map< vector<float>, vector<float> > > expected = {
		{
			{{}, {0,0}},
			{{3.5}, {7}},
			{{22}, {9}}
		},
		{
			{{0.5}, {33}},
			{{5,2}, {99,7}},
			{{22,33,8}, {44}}
		},
		{
			{{1}, {8}},
			{{9}, {66,99,69}}
		}
	};

	auto result = spread(given, 3);

	CHECK( result == expected );
}
