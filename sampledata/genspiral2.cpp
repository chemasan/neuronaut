#include <cmath>
#include <numbers>
#include <random>
#include <iostream>

using namespace std;

int main()
{

    random_device rd;
    mt19937 gen(rd());
    //uniform_real_distribution<float> dist(-1,1);
    normal_distribution<float> dist(0,0.25);

    float radiusStep = 0.03;
    float angleStep = 0.05;

    float radius = 1; 
    float angle = 0;

    float x = sin(angle) * radius;
    float y = cos(angle) * radius;
    
    for (radius = 1, angle=0; x > -9 && x < 9 && y > -9 && y < 9; radius += radiusStep, angle += angleStep)
    {
        x = sin(angle) * radius;
        y = cos(angle) * radius;

        for (size_t i = 0; i < 3; i++)
        {
            float xoffset = dist(gen);
            float yoffset = dist(gen);
            cout << x + xoffset << "," << y + yoffset << "," << "1" << endl;
        }
    }

    radius = 1;
    angle = numbers::pi;

    x = sin(angle) * radius;
    y = cos(angle) * radius;
    for (; x > -9 && x < 9 && y > -9 && y < 9; radius += radiusStep, angle += angleStep)
    {
        x = sin(angle) * radius;
        y = cos(angle) * radius;

        for (size_t i = 0; i < 3; i++)
        {
            float xoffset = dist(gen);
            float yoffset = dist(gen);
            cout << x + xoffset << "," << y + yoffset << "," << "-1" << endl;
        }
    }

    return 0;
}
