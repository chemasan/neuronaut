#include <cmath>
#include <numbers>
#include <random>
#include <iostream>

using namespace std;

int main()
{
    float radiusStep = 0.03;
    float angleStep = 0.05;

    float radius = 1; 
    float angle = 0;

    float x = sin(angle) * radius;
    float y = cos(angle) * radius;
    
    for (radius = 1, angle=0; x > -9 && x < 9 && y > -9 && y < 9; radius += radiusStep, angle += angleStep)
    {
        x = sin(angle) * radius;
        y = cos(angle) * radius;
        cout << x << "," << y << "," << "1" << endl;
    }

    radius = 1;
    angle = numbers::pi;

    x = sin(angle) * radius;
    y = cos(angle) * radius;
    for (; x > -9 && x < 9 && y > -9 && y < 9; radius += radiusStep, angle += angleStep)
    {
        x = sin(angle) * radius;
        y = cos(angle) * radius;
        cout << x << "," << y << "," << "-1" << endl;
    }

    return 0;
}
