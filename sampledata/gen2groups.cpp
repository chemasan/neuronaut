#include <iostream>
#include <cmath>
#include <random>

using namespace std;

int main()
{
    random_device rd;
    mt19937 gen(rd());
    //uniform_real_distribution<float> dist(-10,10);

    normal_distribution<float> dist(0,3);
    
    for (size_t i = 0; i < 500; i++) cout << 7 + dist(gen) << ", " << -3 + dist(gen)<< ", 0" << endl;
    for (size_t i = 0; i < 500; i++) cout << -3 + dist(gen) << ", " << 7 + dist(gen)<< ", 1" << endl;


    return 0;
}
