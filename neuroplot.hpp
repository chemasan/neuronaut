#ifndef _NEUROPLOT_HPP
#define _NEUROPLOT_HPP

#include <map>
#include <vector>
#include <string>
#include <optional>
#include <cstdint>
#include <functional>


namespace neuroplot
{

using namespace std;

class Color
{
	public:
	uint8_t red = 0;
	uint8_t green = 0;
	uint8_t blue = 0;
	uint8_t alpha = 255;
	Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);
	Color(uint32_t pixel);
	uint32_t toInt32() const;
};
const Color WHITE(0xFF,0xFF,0xFF);
const Color BLACK(0,0,0);

const Color RED(0xFF,0,0);
const Color GREEN(0,0xFF,0);	  // aka Lime
const Color BLUE(0,0,0xFF);

const Color YELLOW(0xFF,0xFF,0);
const Color MAGENTA(0xFF,0,0xFF); // aka Fuchsia
const Color CYAN(0,0xFF,0xFF);	// aka Aqua


template<typename T>
class Point
{
public:
	// Properties
	T x = 0;
	T y = 0;

	// Operators
	Point& operator=(const Point& other) = default;
	Point& operator=(Point&& other) = default;

	// Constructors
	Point(T x, T y) : x(x), y(y)
	{
		// Nothing to do
	}
	Point() = default;
	Point(const Point& other) = default;
	Point(Point&& other) = default;
};

using PointInt = Point<int32_t>;
using PointFloat = Point<float>;


//Functions

double distance(PointInt p1, PointInt p2);
double distance(PointFloat p1, PointFloat p2);

class Surface
{
public:

	// Pixel and image manipulations
	void apply(const Surface& other, int16_t x = 0, int16_t y = 0);
	void apply(const Surface& other, int16_t srcX, int16_t srcY, uint16_t srcWidth, uint16_t srcHeight, int16_t x = 0, int16_t y = 0);
	uint32_t getPixel(int32_t x, int32_t y);
	void setPixel(int32_t x, int32_t y, uint32_t pixel);
	void fill(const Color& color);
	void rectangle(int16_t x, int16_t y, uint16_t width, uint16_t height, const Color& color);
	void circle(int32_t cx, int32_t cy, int32_t radius, const Color& color);
	void vline(int32_t x, const Color& color, uint32_t thick = 1);
	void hline(int32_t y, const Color& color, uint32_t thick = 1);
	void line(const PointInt& p1, const PointInt& p2, const Color& color, uint32_t thick = 1);
	Surface flip(bool vertically, bool horizontally);
	Surface clip(int16_t x, int16_t y, uint16_t width, uint16_t height);
	void setColorKey(const Color& color);
	void clearColorKey();

	// Surface information
	int32_t width() const;
	int32_t height() const;

	// Operators
	Surface& operator=(const Surface& other);
	Surface& operator=(Surface&& other);

	// Constructors & destructors
	Surface(int32_t width, int32_t height);  //TODO: shall it be unsigned? but the unerlying function takes signed ones
	Surface(int32_t width, int32_t height, const uint32_t* pixels);
	Surface();
	Surface(const Surface& other);
	Surface(Surface&& other);
	~Surface();

	// Friends
	friend class Display;
	friend class TrueTypeFont;

private:
	Surface(void* data);
	void* data = nullptr;		
};

class TrueTypeFont
{
public:
	// Text rendering
	Surface render(const string& text, const Color& color = WHITE);

	// Builders
	static TrueTypeFont loadFile(const string& filepath, int32_t size = 10);

	// Operators
	TrueTypeFont& operator=(TrueTypeFont&& other);

	// Constructors and destructors
	TrueTypeFont(TrueTypeFont&& other);
	~TrueTypeFont();

	// Not allowed constructors
	TrueTypeFont() = delete;
	TrueTypeFont(const TrueTypeFont&) = delete;
	Surface& operator=(const Surface& other) = delete;

private:
	TrueTypeFont(void* data);
	void* data = nullptr;
};

class Plot
{
public:
	Surface surface;
	optional<TrueTypeFont> font;

	void apply(const Surface& other, float x = 0, float y = 0);
	void text(const string& text, float x, float y, const Color& color = BLACK);
	void drawGrid(float stepx=1, float stepy=1, const Color& color = 0xffcccccc, uint32_t thick = 1);
	void drawAxis(float stepx=1, float stepy=1, const Color& color = 0xffcccccc, uint32_t thick = 5);
	void fill(const Color& color);
	void rectangle(float x, float y, float width, float height, const Color& color);
	void circle(float x, float y, float radius, const Color& color);
	void vline(float x, const Color& color, uint32_t thick = 1);
	void hline(float y, const Color& color, uint32_t thick = 1);
	void line(float x1, float y1, float x2, float y2, const Color& color, uint32_t thick = 1);
	void line(const PointFloat& p1, const PointFloat& p2, const Color& color, uint32_t thick = 1);

	void points(const vector<float>& points, const Color& color, float radius);
	void points(const vector<PointFloat>& points, const Color& color, float radius);
	void points(const map<float,float>& points, const Color& color, float radius);
	void points(function<float(float)> f, float step, const Color& color, float radius);

	void lines(const vector<float>& points, const Color& color, uint32_t thick = 1);
	void lines(const vector<PointFloat>& points, const Color& color, uint32_t thick = 1);
	void lines(const map<float,float>& points, const Color& color, uint32_t thick);
	void lines(function<float(float)> f, float step, const Color& color, uint32_t thick = 1);

	void gradient(const vector<vector<float>>& pixels, const Color& from = BLACK, const Color& to = WHITE, const Color& colorStep = 0xff000000);
	void gradient(const map<PointFloat, float>& pixels, float step, const Color& from = BLACK, const Color& to = WHITE, const Color& colorStep = 0xff000000);
	void gradient(function<float(float,float)> f, float step, const Color& from = BLACK, const Color& to = WHITE, const Color& colorStep = 0xff000000);
	void gradient(function<float(float,float)> f, float step, float minz, float maxz, const Color& from, const Color& to, const Color& colorStep = 0xff000000);

	int32_t translateX(float x);
	int32_t translateY(float y);
	int32_t translateWidth(float width);
	int32_t translateHeight(float height);

	Plot(uint32_t width, uint32_t height, float minx, float maxx, float miny, float maxy);
	Plot(uint32_t width, uint32_t height, float minx, float maxx, float miny, float maxy, const string& fontFile, uint32_t fontSize = 10);

private:
	float minx;
	float maxx;
	float miny;
	float maxy;

};

class Display
{
public:
	// Screen surface
	Surface screen = Surface(nullptr);

	// Display manipulation
	void resize(int32_t width, int32_t height);
	void setFullscreen(bool fullscreen);
	bool getFullScreen() const;
	void setCaption(const string& caption);
	void refresh();

	// Screen surface
	void apply(const Surface& other, int16_t x = 0, int16_t y = 0);
	void apply(const Surface& other, int16_t srcX, int16_t srcY, uint16_t srcWidth, uint16_t srcHeight, int16_t x = 0, int16_t y = 0);
	void apply(const Plot& plot, int16_t x = 0, int16_t y = 0);
	void apply(const Plot& plot, int16_t srcX, int16_t srcY, uint16_t srcWidth, uint16_t srcHeight, int16_t x = 0, int16_t y = 0);
	void fill(const Color& color);
	int32_t width() const;
	int32_t height() const;

	// Constructors and destructors
	Display(const string caption, int32_t width = 640, int32_t height = 480, bool fullscreen = false);
	~Display();

	// Not allowed constructors
	Display() = delete;
	Display(const Display&) = delete;
	Display(Display&&) = delete;

private:
	bool fullScreen = false;
};

}

#endif
