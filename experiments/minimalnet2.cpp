#include "../neuronaut.hpp"
#include "../neuroplot.hpp"
#include <iostream>
#include <vector>
#include <functional>
#include <chrono>
#include <csignal>

using namespace std;
using namespace neuronaut;
using namespace neuroplot;

/*
    Example of a neural network with a fixed topology 2,3,1.

    This example shows the minimal code to necessary to have a network that works,
    including the trainning process with the backpropagation and gradient descend algorithms.

    It has been avoided to model individual neurons and avoided any loop inside the forwardPropagation and trainning functions,
    the so it is easier to follow the calculations inside the network.
*/

function<float(float)> activation = neuronaut::tanh;
function<float(float)> activationDerivative = tanhDerivative;
function<float(float, float)> loss = halfMse;
function<float(float,float)>lossDeriv = halfMseDerivative;

vector<vector<float>> values = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<float>> biases = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<vector<float>>> weights = {
    {{0, 0, 0}, {0, 0, 0}},    // Layer 0 to 1
    {{0}, {0}, {0}},           // Layer 1 to 2
};


vector<vector<float>> biasesGradient = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<vector<float>>> weightsGradient = {
    {{0, 0, 0}, {0, 0, 0}},    // Layer 0 to 1
    {{0}, {0}, {0}},           // Layer 1 to 2
};


vector<vector<float>> biasesGradientAcc = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<vector<float>>> weightsGradientAcc = {
    {{0, 0, 0}, {0, 0, 0}},    // Layer 0 to 1
    {{0}, {0}, {0}},           // Layer 1 to 2
};

float acc = 0;

float forwardPropagation(float input0, float input1)
{
    values[0][0] = input0 + biases[0][0];
    values[0][1] = input1 + biases[0][1];

    values[1][0] = activation( (values[0][0] * weights[0][0][0]) + (values[0][1] * weights[0][1][0]) + biases[1][0] );
    values[1][1] = activation( (values[0][0] * weights[0][0][1]) + (values[0][1] * weights[0][1][1]) + biases[1][1] );
    values[1][2] = activation( (values[0][0] * weights[0][0][2]) + (values[0][1] * weights[0][1][2]) + biases[1][2] );

    values[2][0] = activation( (values[1][0] * weights[1][0][0]) + (values[1][1] * weights[1][1][0]) + (values[1][2] * weights[1][2][0]) + biases[2][0] );
    
    return values[2][0];
}

void backwardPropagation(float result, float expected)
{
    float totalLossDeriv = lossDeriv(expected, result);

    float neuronError = totalLossDeriv * activationDerivative((values[1][0] * weights[1][0][0]) + (values[1][1] * weights[1][1][0]) + (values[1][2] * weights[1][2][0]) + biases[2][0] );
    biasesGradient[2][0] = neuronError;
    weightsGradient[1][0][0] = neuronError * values[1][0];
    weightsGradient[1][1][0] = neuronError * values[1][1];
    weightsGradient[1][2][0] = neuronError * values[1][2];

    neuronError = biasesGradient[2][0] * weights[1][0][0] * activationDerivative((values[0][0] * weights[0][0][0]) + (values[0][1] * weights[0][1][0]) + biases[1][0] );
    biasesGradient[1][0] = neuronError;
    weightsGradient[0][0][0] = neuronError * values[0][0];
    weightsGradient[0][1][0] = neuronError * values[0][1];

    neuronError = biasesGradient[2][0] * weights[1][1][0] * activationDerivative( (values[0][0] * weights[0][0][1]) + (values[0][1] * weights[0][1][1]) + biases[1][1] );
    biasesGradient[1][1] = neuronError;
    weightsGradient[0][0][1] = neuronError * values[0][0];
    weightsGradient[0][1][1] = neuronError * values[0][1];

    neuronError = biasesGradient[2][0] * weights[1][2][0] * activationDerivative( (values[0][0] * weights[0][0][2]) + (values[0][1] * weights[0][1][2]) + biases[1][2] );
    biasesGradient[1][2] = neuronError;
    weightsGradient[0][0][2] = neuronError * values[0][0];
    weightsGradient[0][1][2] = neuronError * values[0][1];

    neuronError = (biasesGradient[1][0] * weights[0][0][0]) + (biasesGradient[1][1] * weights[0][0][1]) + (biasesGradient[1][2] * weights[0][0][2]);
    biasesGradient[0][0] = neuronError;
    neuronError = (biasesGradient[1][0] * weights[0][1][0]) + (biasesGradient[1][1] * weights[0][1][1]) + (biasesGradient[1][2] * weights[0][1][2]);
    biasesGradient[0][1] = neuronError;

    biasesGradientAcc[0][0] += biasesGradient[0][0];
    biasesGradientAcc[0][1] += biasesGradient[0][1];
    biasesGradientAcc[1][0] += biasesGradient[1][0];
    biasesGradientAcc[1][1] += biasesGradient[1][1];
    biasesGradientAcc[1][2] += biasesGradient[1][2];
    biasesGradientAcc[2][0] += biasesGradient[2][0];
    weightsGradientAcc[0][0][0] += weightsGradient[0][0][0];
    weightsGradientAcc[0][0][1] += weightsGradient[0][0][1];
    weightsGradientAcc[0][0][2] += weightsGradient[0][0][2];
    weightsGradientAcc[0][1][0] += weightsGradient[0][1][0];
    weightsGradientAcc[0][1][1] += weightsGradient[0][1][1];
    weightsGradientAcc[0][1][2] += weightsGradient[0][1][2];
    weightsGradientAcc[1][0][0] += weightsGradient[1][0][0];
    weightsGradientAcc[1][1][0] += weightsGradient[1][1][0];
    weightsGradientAcc[1][2][0] += weightsGradient[1][2][0];
    acc++;
}

void gradientDescend(float learningRate)
{
    biases[0][0]     -= learningRate * (biasesGradientAcc[0][0]/acc);
    weights[0][0][0] -= learningRate * (weightsGradientAcc[0][0][0]/acc);
    weights[0][0][1] -= learningRate * (weightsGradientAcc[0][0][1]/acc);
    weights[0][0][2] -= learningRate * (weightsGradientAcc[0][0][2]/acc);
    biases[0][1]     -= learningRate * (biasesGradientAcc[0][1]/acc);
    weights[0][1][0] -= learningRate * (weightsGradientAcc[0][1][0]/acc);
    weights[0][1][1] -= learningRate * (weightsGradientAcc[0][1][1]/acc);
    weights[0][1][2] -= learningRate * (weightsGradientAcc[0][1][2]/acc);
    biases[1][0]     -= learningRate * (biasesGradientAcc[1][0]/acc);
    weights[1][0][0] -= learningRate * (weightsGradientAcc[1][0][0]/acc);
    biases[1][1]     -= learningRate * (biasesGradientAcc[1][1]/acc);
    weights[1][1][0] -= learningRate * (weightsGradientAcc[1][1][0]/acc);
    biases[1][2]     -= learningRate * (biasesGradientAcc[1][2]/acc);
    weights[1][2][0] -= learningRate * (weightsGradientAcc[1][2][0]/acc);
    biases[2][0]     -= learningRate * (biasesGradientAcc[2][0]/acc);

    acc = 0;
    biasesGradientAcc[0][0] = 0;
    biasesGradientAcc[0][1] = 0;
    biasesGradientAcc[1][0] = 0;
    biasesGradientAcc[1][1] = 0;
    biasesGradientAcc[1][2] = 0;
    biasesGradientAcc[2][0] = 0;
    weightsGradientAcc[0][0][0] = 0;
    weightsGradientAcc[0][0][1] = 0;
    weightsGradientAcc[0][0][2] = 0;
    weightsGradientAcc[0][1][0] = 0;
    weightsGradientAcc[0][1][1] = 0;
    weightsGradientAcc[0][1][2] = 0;
    weightsGradientAcc[1][0][0] = 0;
    weightsGradientAcc[1][1][0] = 0;
    weightsGradientAcc[1][2][0] = 0;
}

void trainBatch(float learningRate, const vector<vector<float>>& samples)
{
    for (auto& sample : samples)
    {
        float input0 = sample[0];
        float input1 = sample[1];
        float expectedOutput = sample[2];
        float output = forwardPropagation(input0, input1);
        backwardPropagation(output, expectedOutput);
    }
    gradientDescend(learningRate);
}

float quadraticLoss(const vector<vector<float>>& samples)
{
    float sum = 0;
    for (auto& sample : samples)
    {
        float input0 = sample[0];
        float input1 = sample[1];
        float expected = sample[2];
        float output = forwardPropagation(input0,input1);
        sum += loss(expected, output);
    }
    return sum/samples.size();
}


void printData(Plot& plot, vector<vector<float>> samples)
{
    for(auto& sample : samples)
    {
        plot.circle(sample[0],sample[1], 0.3, BLACK);
        plot.circle(sample[0],sample[1], 0.2, sample[2] > 0 ? RED : BLUE);
    }
}

vector<vector<float>> shuffle(vector<vector<float>> v)
{
    vector<vector<float>> ret;
    Random r;
    while (v.size() > 0)
    {
        size_t i = r.get<size_t>(0, v.size() - 1);
        ret.emplace_back(v[i]);
        v.erase(v.begin() + i);
    }
    return ret;
}

bool keepRunning = true;
void sigHandler(int signal)
{
    keepRunning = false;
}

int main()
{
    ios::sync_with_stdio(false);
    signal(SIGINT, sigHandler);

    Random r;
    for (size_t l = 0; l < biases.size(); l++)
    {
        for(size_t n = 0; n < biases[l].size(); n++)
        {
            biases[l][n] = r(-1,1);
        }
    }
    for (size_t l = 0; l < weights.size(); l++)
    {
        for(size_t n = 0; n < weights[l].size(); n++)
        {
            for (size_t c = 0; c < weights[l][n].size(); c++) weights[l][n][c] = r(-1,1);
        }
    }

    auto samples = loadFloatCsv("../sampledata/circles.csv");
    vector<vector<float>> trainData;
    vector<vector<float>> testData;

    for (size_t i = 0; i < samples.size(); i++)
    {
        if (i % 4 == 0) testData.push_back(samples[i]);
        else trainData.push_back(samples[i]);
    }

    cout << "Trainning samples: " << trainData.size() << endl;
    cout << "Testing samples: " << testData.size() << endl;
    cout << endl;
    cout << "Initial trainning loos: " << quadraticLoss(trainData) << endl;
    cout << "Initial testing loss: " << quadraticLoss(testData) << endl;
    cout << endl;

    Display display("example", 800,600);
    Plot plot(display.width(), display.height(), -10, 10.01, -10.01, 10);
    plot.gradient(forwardPropagation, 0.1, BLUE, RED);
    printData(plot, samples);
    display.apply(plot);
    display.refresh();

    auto startTime = chrono::steady_clock::now();
    auto lastRefresh = startTime;
    auto lastPrint = startTime;

    trainData = shuffle(trainData);
    size_t epoch = 0;
    size_t cursample = 0;
    while(keepRunning)
    {
        auto curTime = chrono::steady_clock::now();
        chrono::milliseconds refreshMillis = chrono::duration_cast<chrono::milliseconds>(curTime - lastRefresh);
        chrono::milliseconds printMillis   = chrono::duration_cast<chrono::milliseconds>(curTime - lastPrint);
        chrono::milliseconds runningTime   = chrono::duration_cast<chrono::milliseconds>(curTime - startTime);

        if (cursample >=trainData.size())
        {
            cursample = 0;
            trainData = shuffle(trainData);
        }
        vector<vector<float>> batch;
        for (size_t i = 0; i<3 && cursample < trainData.size(); i++)
        {
            batch.push_back(trainData[cursample++]);
        }
        trainBatch(0.03, batch);
        epoch++;

        if (runningTime.count() > 6000000) break;  // Run for 10 minutes maximum
        if (printMillis.count() >   1000)
        {
            cout << "Epoch: " << epoch;
            cout << " Trainning loos: " << quadraticLoss(trainData);
            cout << ", testing loss: "  << quadraticLoss(testData) << endl;
            lastPrint = chrono::steady_clock::now();
        }
        if (refreshMillis.count() > 33)
        {
            plot.gradient(forwardPropagation, 0.1, BLUE, RED);
            plot.drawGrid();
            plot.drawAxis();
            printData(plot, samples);
            display.apply(plot);
            display.refresh();
            lastRefresh = chrono::steady_clock::now();
        }
    }

    cout << endl;
    cout << "Epoc: " << epoch << endl;
    cout << "Final trainning loos: " << quadraticLoss(trainData) << endl;
    cout << "Final testing loss:   " << quadraticLoss(testData)  << endl;

    if (keepRunning) getchar();

    return 0;
}
