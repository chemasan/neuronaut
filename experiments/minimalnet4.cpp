#include "../neuronaut.hpp"
#include "../neuroplot.hpp"
#include <iostream>
#include <vector>
#include <functional>
#include <chrono>
#include <csignal>
#include <thread>

using namespace std;
using namespace neuronaut;
using namespace neuroplot;


// Multi Layer Perceptron class
using Topology = vector<size_t>;
class MLP
{
public:
    vector<vector<float>> inputs;
    vector<vector<float>> outputs;
    vector<vector<float>> biases;
    vector<vector<vector<float>>> weights;
    vector<vector<ActivationFunction>> activations;

    size_t countNeurons();
    size_t countWeights();
    void setParams(vector<float> biases, vector<float> weights);
    vector<float> propagateForward(const vector<float>& vector);

    MLP(const Topology& topology, ActivationFunction activation);
};


size_t MLP::countNeurons()
{
    size_t ret = 0;
    for (size_t i = 0; i < inputs.size(); i++) ret += inputs[i].size();
    return ret;
}

size_t MLP::countWeights()
{
    size_t ret = 0;
    for (size_t l = 0; l < weights.size() - 1; l++)
    {
        for (size_t n = 0; n < weights[l].size(); n++)
        {
            ret += weights[l][n].size();
        }
    }
    return ret;
}

void MLP::setParams(vector<float> biases, vector<float> weights)
{
    if (biases.size() != countNeurons()) throw invalid_argument("Provided biases doesn't match the amount of neurons in the network");
    if (weights.size() != countWeights()) throw invalid_argument("Provided weights doesn't match the amount of weights in the network");

    size_t last = this->biases.size() - 1;

    size_t b = 0;
    size_t w = 0;

    for (size_t l = 0; l < this->biases.size(); l++)
    {
        for (size_t n = 0; n < this->biases[l].size(); n++)
        {
            this->biases[l][n] = biases[b++];
            if (n >= last) continue;
            for (size_t c = 0; c < this->weights[l][n].size(); c++)
            {
                this->weights[l][n][c] = weights[w++];
            }
        }
    }
}

vector<float> MLP::propagateForward(const vector<float>& netInput)
{
    if (netInput.size() != inputs[0].size()) throw invalid_argument("inputs count doesn't match the input layer");

    for (size_t i = 0; i < inputs[0].size(); i++)
    {
        inputs[0][i] = netInput[i] + biases[0][i];
        outputs[0][i] = activations[0][i](inputs[0][i]); 
    }

    for (size_t layer = 1; layer < inputs.size(); layer++)
    {
        for (size_t neuron = 0; neuron < inputs[layer].size(); neuron++)
        {
            inputs[layer][neuron] = biases[layer][neuron];
            for (size_t prevNeuron = 0; prevNeuron < outputs[layer-1].size(); prevNeuron++)
            {
                inputs[layer][neuron] += outputs[layer-1][prevNeuron] * weights[layer-1][prevNeuron][neuron];
            }
            outputs[layer][neuron] = activations[layer][neuron](inputs[layer][neuron]);
        }
    }
    return outputs.back();
}

MLP::MLP(const Topology& topology, ActivationFunction activation)
{
    inputs = {};
    outputs = {};
    biases = {};
    activations = {};
    weights = {};

    for (size_t layer = 0; layer < topology.size(); layer++)
    {
        size_t neurons = topology[layer];
        inputs.emplace_back(vector<float>(neurons, 0));
        outputs.emplace_back(vector<float>(neurons, 0));
        biases.emplace_back(vector<float>(neurons, 0));
        if (layer == 0)
        {
            activations.emplace_back(vector<function<float(float)>>(neurons,linear));
            weights.emplace_back(vector<vector<float>>(neurons, vector<float>(topology[layer+1],0) ));
        }
        else if (layer == topology.size() - 1)
        {
            activations.emplace_back(vector<function<float(float)>>(neurons,linear));
        }
        else
        {
            activations.emplace_back(vector<function<float(float)>>(neurons,activation));
            weights.emplace_back(vector<vector<float>>(neurons, vector<float>(topology[layer+1],0) ));
        }
    }

    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<float> dist(-1,1);
    for (size_t layer = 0; layer < topology.size(); layer++)
    {
        size_t neurons = topology[layer];
        for (size_t neuron = 0; neuron < neurons; neuron++ )
        {
            biases[layer][neuron] = dist(gen);
            for (size_t nextNeuron = 0; layer < topology.size() - 1 && nextNeuron < topology[layer + 1]; nextNeuron++)
            {
                weights[layer][neuron][nextNeuron]  = dist(gen);
            }
        }
    }
}


bool keepRunning = true;
void sigHandler(int signal)
{
    keepRunning = false;
}

int main()
{
    ios::sync_with_stdio(false);
    signal(SIGINT, sigHandler);

    MLP net({1,3,1}, neuronaut::relu);

    vector<vector<float>> biases = {
    {0,0},              // Layer 0 (INPUT)
    {0.38,0.82,0.34},   // Layer 1
    {0}                 // Lyaer 2 (OUTPUT)
    };
    
    vector<vector<vector<float>>> weights = {
        {{1.4, -0.46, -0.85}, {0.65, -0.98, -0.21}},    // Layer 0 to 1
        {{1.6}, {-1.3}, {-0.87}},                       // Layer 1 to 2
    };

    net.biases = biases;
    net.weights = weights;

    Display display("example", 800,600);
    Plot plot(display.width(), display.height(), -10, 10.01, -10.01, 10);
    plot.drawGrid();
    plot.drawAxis();

    plot.lines([&](float x){ return net.propagateForward({x})[0]; }, 1, BLUE, 3);

    display.apply(plot);
    display.refresh();


    if (keepRunning) getchar();

    return 0;
}
