#include "../neuronaut.hpp"
#include "../neuroplot.hpp"
#include <iostream>
#include <vector>
#include <functional>
#include <chrono>
#include <csignal>
#include <thread>

using namespace std;
using namespace neuronaut;
using namespace neuroplot;

/*
    Example of a neural network with a flexible topology that can be set to an arbitrary number of neurons and layers,
    including the trainning process with the backpropagation and gradient descend algorithms.

    It has been avoided to model individual neurons.
*/

// Hyperparameters
const float LEARNINGRATE = 0.003;
const size_t BATCHSIZE = 10;
const vector<size_t> TOPOLOGY = {2, 10, 10, 5, 1};
function<float(float)> activation = neuronaut::tanh;
function<float(float)> activationDerivative = tanhDerivative;
function<float(float, float)> loss = halfMse;
function<float(float,float)>lossDeriv = halfMseDerivative;

// Network components
vector<vector<float>> inputs = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<float>> outputs = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<float>> biases = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<vector<float>>> weights = {
    {{0, 0, 0}, {0, 0, 0}},    // Layer 0 to 1
    {{0}, {0}, {0}},           // Layer 1 to 2
};

vector<vector<function<float(float)>>> activations = {
    {linear, linear},
    {activation,activation,activation},
    {neuronaut::tanh}
};
vector<vector<function<float(float)>>> activationsDerivatives = {
    {linearDerivative, linearDerivative},
    {activationDerivative,activationDerivative,activationDerivative},
    {tanhDerivative}
};

vector<vector<float>> biasesGradient = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<vector<float>>> weightsGradient = {
    {{0, 0, 0}, {0, 0, 0}},    // Layer 0 to 1
    {{0}, {0}, {0}},           // Layer 1 to 2
};


vector<vector<float>> biasesGradientAcc = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<vector<float>>> weightsGradientAcc = {
    {{0, 0, 0}, {0, 0, 0}},    // Layer 0 to 1
    {{0}, {0}, {0}},           // Layer 1 to 2
};

float acc = 0;

float forwardPropagation(float input0, float input1)
{
    inputs[0][0] = input0 + biases[0][0];
    inputs[0][1] = input1 + biases[0][1];
    outputs[0][0] = inputs[0][0];
    outputs[0][1] = inputs[0][1];

    for (size_t layer = 1; layer < inputs.size(); layer++)
    {
        for (size_t neuron = 0; neuron < inputs[layer].size(); neuron++)
        {
            inputs[layer][neuron] = biases[layer][neuron];
            for (size_t prevNeuron = 0; prevNeuron < outputs[layer-1].size(); prevNeuron++)
            {
                inputs[layer][neuron] += outputs[layer-1][prevNeuron] * weights[layer-1][prevNeuron][neuron];
            }
            outputs[layer][neuron] = activations[layer][neuron](inputs[layer][neuron]);
        }
    }
    return outputs[outputs.size()-1][0];
}

void backwardPropagation(float result, float expected)
{
    float totalLossDeriv = lossDeriv(expected, result);

    size_t lastLayer = inputs.size() - 1;
    for (size_t neuron = 0; neuron < inputs.back().size(); neuron++)
    {
        biasesGradient[lastLayer][neuron] = totalLossDeriv;
        biasesGradientAcc[lastLayer][neuron] += biasesGradient[lastLayer][neuron];
    }

    for (size_t layer = lastLayer; layer > 0; layer--)
    {
        for (size_t neuron = 0; neuron < inputs[layer].size(); neuron++)
        {
            size_t prevLayer = layer - 1;
            biasesGradient[layer][neuron] = biasesGradient[layer][neuron] * activationsDerivatives[layer][neuron](inputs[layer][neuron]);
            biasesGradientAcc[layer][neuron] += biasesGradient[layer][neuron];
            for (size_t prevNeuron = 0; prevNeuron < inputs[prevLayer].size(); prevNeuron++)
            {
                weightsGradient[prevLayer][prevNeuron][neuron] = biasesGradient[layer][neuron] * outputs[prevLayer][prevNeuron];
                weightsGradientAcc[prevLayer][prevNeuron][neuron] += weightsGradient[prevLayer][prevNeuron][neuron];
                if (neuron == 0) biasesGradient[prevLayer][prevNeuron] = 0;
                biasesGradient[prevLayer][prevNeuron] += biasesGradient[layer][neuron] * weights[prevLayer][prevNeuron][neuron];
            }
        }
    }
    for (size_t neuron = 0; neuron < inputs[0].size(); neuron++)
    {
        biasesGradientAcc[0][neuron] += biasesGradient[0][neuron];
    }
    acc++;
}

void gradientDescend(float learningRate)
{
    size_t lastLayer = biases.size() - 1;
    for (size_t layer = 0; layer <= lastLayer; layer++)
    {
        for (size_t neuron = 0; neuron < biasesGradientAcc[layer].size(); neuron++)
        {
            biases[layer][neuron] -= learningRate * (biasesGradientAcc[layer][neuron]/acc);
            biasesGradientAcc[layer][neuron] = 0;
            if (layer >= lastLayer) continue;
            for (size_t nextNeuron = 0; nextNeuron < weightsGradientAcc[layer][neuron].size(); nextNeuron++)
            {
                weights[layer][neuron][nextNeuron] -= learningRate * (weightsGradientAcc[layer][neuron][nextNeuron]/acc);
                weightsGradientAcc[layer][neuron][nextNeuron] = 0;
            }
        }
    }
    acc = 0;
}

void trainBatch(float learningRate, const vector<vector<float>>& samples)
{
    size_t lastLayer = biases.size() - 1;

    for (auto& sample : samples)
    {
        float input0 = sample[0];
        float input1 = sample[1];
        float expectedOutput = sample[2];
        float output = forwardPropagation(input0, input1);
        backwardPropagation(output, expectedOutput);
    }
    gradientDescend(learningRate);
}

void rebuildNetwork(const vector<size_t>& topology, const function<float(float)> activation, const function<float(float)> activationDerivative)
{
    inputs = {};
    outputs = {};
    biases = {};
    biasesGradient = {};
    biasesGradientAcc = {};
    activations = {};
    activationsDerivatives = {};
    weights = {};
    weightsGradient = {};
    weightsGradientAcc = {};

    for (size_t layer = 0; layer < topology.size(); layer++)
    {
        size_t neurons = topology[layer];
        inputs.emplace_back(vector<float>(neurons, 0));
        outputs.emplace_back(vector<float>(neurons, 0));
        biases.emplace_back(vector<float>(neurons, 0));
        biasesGradient.emplace_back(vector<float>(neurons, 0));
        biasesGradientAcc.emplace_back(vector<float>(neurons, 0));
        if (layer == 0)
        {
            activations.emplace_back(vector<function<float(float)>>(neurons,linear));
            activationsDerivatives.emplace_back(vector<function<float(float)>>(neurons,linearDerivative));

            weights.emplace_back(vector<vector<float>>(neurons, vector<float>(topology[layer+1],0) ));
            weightsGradient.emplace_back(vector<vector<float>>(neurons, vector<float>(topology[layer+1],0) ));
            weightsGradientAcc.emplace_back(vector<vector<float>>(neurons, vector<float>(topology[layer+1],0) ));
        }
        else if (layer == topology.size() - 1)
        {
            activations.emplace_back(vector<function<float(float)>>(neurons,neuronaut::tanh));
            activationsDerivatives.emplace_back(vector<function<float(float)>>(neurons,neuronaut::tanhDerivative));
        }
        else
        {
            activations.emplace_back(vector<function<float(float)>>(neurons,activation));
            activationsDerivatives.emplace_back(vector<function<float(float)>>(neurons,activationDerivative));

            weights.emplace_back(vector<vector<float>>(neurons, vector<float>(topology[layer+1],0) ));
            weightsGradient.emplace_back(vector<vector<float>>(neurons, vector<float>(topology[layer+1],0) ));
            weightsGradientAcc.emplace_back(vector<vector<float>>(neurons, vector<float>(topology[layer+1],0) ));
        }
    }

    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<float> dist(-1,1);
    for (size_t layer = 0; layer < topology.size(); layer++)
    {
        size_t neurons = topology[layer];
        for (size_t neuron = 0; neuron < neurons; neuron++ )
        {
            biases[layer][neuron] = dist(gen);
            for (size_t nextNeuron = 0; layer < topology.size() - 1 && nextNeuron < topology[layer + 1]; nextNeuron++)
            {
                weights[layer][neuron][nextNeuron]  = dist(gen);
            }
        }
    }
}

float quadraticLoss(const vector<vector<float>>& samples)
{
    float sum = 0;
    for (auto& sample : samples)
    {
        float input0 = sample[0];
        float input1 = sample[1];
        float expected = sample[2];
        float output = forwardPropagation(input0,input1);
        sum += loss(expected, output);
    }
    return sum/samples.size();
}


void printData(Plot& plot, vector<vector<float>> samples)
{
    for(auto& sample : samples)
    {
        plot.circle(sample[0],sample[1], 0.3, BLACK);
        plot.circle(sample[0],sample[1], 0.2, sample[2] > 0 ? RED : BLUE);
    }
}

vector<vector<float>> shuffle(vector<vector<float>> v)
{
    vector<vector<float>> ret;
    Random r;
    while (v.size() > 0)
    {
        size_t i = r.get<size_t>(0, v.size() - 1);
        ret.emplace_back(v[i]);
        v.erase(v.begin() + i);
    }
    return ret;
}

bool keepRunning = true;
void sigHandler(int signal)
{
    keepRunning = false;
}

int main()
{
    ios::sync_with_stdio(false);
    signal(SIGINT, sigHandler);

    rebuildNetwork(TOPOLOGY, activation, activationDerivative);

    auto samples = loadFloatCsv("../sampledata/spiral2.csv");
    vector<vector<float>> trainData;
    vector<vector<float>> testData;

    for (size_t i = 0; i < samples.size(); i++)
    {
        if (i % 4 == 0) testData.push_back(samples[i]);
        else trainData.push_back(samples[i]);
    }

    cout << "Trainning samples: " << trainData.size() << endl;
    cout << "Testing samples: " << testData.size() << endl;
    cout << endl;
    cout << "Initial trainning loss: " << quadraticLoss(trainData) << endl;
    cout << "Initial testing loss: " << quadraticLoss(testData) << endl;
    cout << endl;

    Display display("example", 800,600);
    Plot plot(display.width(), display.height(), -10, 10.01, -10.01, 10);
    plot.gradient(forwardPropagation, 0.2, BLUE, RED);
    plot.drawGrid();
    plot.drawAxis();
    printData(plot, samples);
    display.apply(plot);
    display.refresh();

    this_thread::sleep_for(chrono::milliseconds(500));

    auto startTime = chrono::steady_clock::now();
    auto lastRefresh = startTime;
    auto lastPrint = startTime;

    trainData = shuffle(trainData);
    size_t epoch = 0;
    size_t iteration = 0;
    size_t cursample = 0;
    while(keepRunning)
    {
        auto curTime = chrono::steady_clock::now();
        chrono::milliseconds refreshMillis = chrono::duration_cast<chrono::milliseconds>(curTime - lastRefresh);
        chrono::milliseconds printMillis   = chrono::duration_cast<chrono::milliseconds>(curTime - lastPrint);
        chrono::milliseconds runningTime   = chrono::duration_cast<chrono::milliseconds>(curTime - startTime);
        if (cursample >=trainData.size())
        {
            cursample = 0;
            trainData = shuffle(trainData);
            epoch++;
        }
        vector<vector<float>> batch;
        for (size_t i = 0; i<BATCHSIZE && cursample < trainData.size(); i++)
        {
            batch.push_back(trainData[cursample++]);
        }
        trainBatch(LEARNINGRATE, batch);
        iteration++;
        if (runningTime.count() > 6000000) break;   // Run for 10 minutes maximum
        if (printMillis.count() >   2000)
        {
            cout << "Iteration: " << iteration;
            cout << ", Epoch: " << epoch;
            cout << ", Trainning loos: " << quadraticLoss(trainData);
            cout << ", testing loss: "  << quadraticLoss(testData) << endl;
            lastPrint = chrono::steady_clock::now();
        }
        if (refreshMillis.count() > 500)
        {
            plot.gradient(forwardPropagation, 0.2, BLUE, RED);
            plot.drawGrid();
            plot.drawAxis();
            printData(plot, samples);
            display.apply(plot);
            display.refresh();
            lastRefresh = chrono::steady_clock::now();
        }
    }

    cout << endl;
    cout << "Epoc: " << epoch << endl;
    cout << "Final trainning loos: " << quadraticLoss(trainData) << endl;
    cout << "Final testing loss:   " << quadraticLoss(testData)  << endl;

    if (keepRunning) getchar();

    return 0;
}
