#include <iostream>
#include <vector>
#include <stdexcept>
#include <cmath>
#include <iomanip>

using namespace std;

using Vector = vector<float>;

const Vector vline = {
	0,1,0,
	0,1,0,
	0,1,0
};

const Vector hline = {
	0,0,0,
	1,1,1,
	0,0,0
};

const Vector ldiagonal = {
	1,0,0,
	0,1,0,
	0,0,1
};

const Vector rdiagonal = {
	0,0,1,
	0,1,0,
	1,0,0
};

const Vector cross = {
	0,1,0,
	1,1,1,
	0,1,0
};

const Vector tlcorner = {
	0,0,0,
	0,1,1,
	0,1,0
};

const Vector trcorner = {
	0,0,0,
	1,1,0,
	0,1,0
};

const Vector llcorner = {
	0,1,0,
	0,1,1,
	0,0,0
};

const Vector lrcorner = {
	0,1,0,
	1,1,0,
	0,0,0
};


//Vector weights = {
//	-10,-10,-10,
//	 10, 10, 10,
//	-10,-10,-10
//};

Vector weights = {
	-10, -10,  10,
	-10,  10, -10,
	 10, -10, -10
};


float bias = -20;

float relu(float x)
{
	return x > 0 ? x : 0;
}

float sigmoid(float x)
{
        return 1/(1+exp(-x));
}

float predict(const Vector& input)
{
	if (input.size() != 9) throw invalid_argument("input not the right size");

	float result = bias;
	for (size_t i = 0; i < 9; i++)
	{
		result += input[i] * weights[i];
	}
	return sigmoid(result);
}

int main()
{
	cout << setprecision(8) << fixed << showpoint;
	cout << "hline: " << predict(hline) << endl;
	cout << "vline: " << predict(vline) << endl;
	cout << "cross: " << predict(cross) << endl;
	cout << "ldiagonal: " << predict(ldiagonal) << endl;
	cout << "rdiagonal: " << predict(rdiagonal) << endl;
	cout << "tlcorner: " << predict(tlcorner) << endl;
	cout << "lrcorner: " << predict(lrcorner) << endl;
	return 0;
}
