#include <iostream>
#include <vector>
#include <stdexcept>
#include <cmath>
#include <iomanip>

using namespace std;

using Vector = vector<float>;

const Vector vline = {
	0,1,0,
	0,1,0,
	0,1,0
};

const Vector hline = {
	0,0,0,
	1,1,1,
	0,0,0
};

const Vector ldiagonal = {
	1,0,0,
	0,1,0,
	0,0,1
};

const Vector rdiagonal = {
	0,0,1,
	0,1,0,
	1,0,0
};

const Vector cross = {
	0,1,0,
	1,1,1,
	0,1,0
};

const Vector tlcorner = {
	0,0,0,
	0,1,1,
	0,1,0
};

const Vector trcorner = {
	0,0,0,
	1,1,0,
	0,1,0
};

const Vector llcorner = {
	0,1,0,
	0,1,1,
	0,0,0
};

const Vector lrcorner = {
	0,1,0,
	1,1,0,
	0,0,0
};

Vector weights0 = {
	-10, 10,-10,
	-10, 10,-10,
	-10, 10,-10
};


Vector weights1 = {
	-10,-10,-10,
	 10, 10, 10,
	-10,-10,-10
};

Vector weights2 = {
	 10, -10, -10,
	-10,  10, -10,
	-10, -10,  10
};

Vector weights3 = {
	-10, -10,  10,
	-10,  10, -10,
	 10, -10, -10
}

;Vector weights4 = {
	-10, -10, -10,
	-10,  10,  10,
	-10,  10, -10
};

Vector weights5 = {
	-10, -10, -10,
	 10,  10, -10,
	-10,  10, -10
};

Vector weights6 = {
	-10,  10, -10,
	-10,  10,  10,
	-10, -10, -10
};

Vector weights7 = {
	-10,  10, -10,
	 10,  10, -10,
	-10, -10, -10
};

Vector weights8 = {
	-10,  10, -10,
	 10,  10,  10,
	-10,  10, -10
};

vector<Vector> weightsl0l1 = {weights0, weights1, weights2, weights3, weights4, weights5, weights6, weights7, weights8};

vector<Vector> weightsl1l2 = {
	{ 8,-8,-8,-8,-8,-8,-8,-8,-8},
	{-8, 8,-8,-8,-8,-8,-8,-8,-8},
	{-8,-8, 8,-8,-8,-8,-8,-8,-8},
	{-8,-8,-8, 8,-8,-8,-8,-8,-8},
	{-8,-8,-8,-8, 8,-8,-8,-8,-8},
	{-8,-8,-8,-8,-8, 8,-8,-8,-8},
	{-8,-8,-8,-8,-8,-8, 8,-8,-8},
	{-8,-8,-8,-8,-8,-8,-8, 8,-8},
	{-16,-16,-16,-16,-16,-16,-16,-16, 8}
};

float bias = -20;
float bias2 = 0.8;

float relu(float x)
{
	return x > 0 ? x : 0;
}

float sigmoid(float x)
{
        return 1/(1+exp(-x));
}

Vector predict(const Vector& input)
{
	if (input.size() != 9) throw invalid_argument("input not the right size");

	Vector retl0l1;
	for (size_t dn = 0; dn < 9; dn++)
	{
		float result = bias;
		for (size_t sn = 0; sn < 9; sn++)
		{
			result += input[sn] * weightsl0l1[dn][sn];
		}
		retl0l1.emplace_back(sigmoid(result));
	}
	Vector retl1l2;
	for (size_t dn = 0; dn < 9; dn++)
	{
		float result = bias2;
		for (size_t sn = 0; sn < 9; sn++)
		{
			result += retl0l1[sn] * weightsl1l2[dn][sn];
		}
		retl1l2.emplace_back(sigmoid(result));
	}
	return retl1l2;
}

ostream& operator << (ostream& os, const Vector& v)
{
	os << "[";
	for (size_t i = 0; i < v.size(); i++)
	{
		if (i > 0) os << ", ";
		os << i << ":" << v[i];
	}
	os << "]";
	return os;
}

int main()
{
	cout << setprecision(8) << fixed << showpoint;
	cout << "hline: " << predict(hline) << endl;
	cout << "vline: " << predict(vline) << endl;
	cout << "cross: " << predict(cross) << endl;
	cout << "ldiagonal: " << predict(ldiagonal) << endl;
	cout << "rdiagonal: " << predict(rdiagonal) << endl;
	cout << "tlcorner: " << predict(tlcorner) << endl;
	cout << "lrcorner: " << predict(lrcorner) << endl;
	return 0;
}
