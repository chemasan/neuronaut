#include <iostream>
#include <vector>
#include <functional>

using namespace std;

/*
    Example of a neural network with a fixed topology 2,3,1.
    That is 2 neurons in the input layer, 1 hidden layer with 3 neurons, and 1 neuron in the output layer

    This example shows the minimal code to necessary to have a minimal network that works.
    For simplicity, it has been avoided to model individual neurons, it has been avoided the trainning process,
    and also avoided any loop inside the forwardPropagation function so it is easier to follow the calculations inside the network.
*/

function<float(float)> activation = [](float x){ return x<0 ? 0 : x ; }; // Relu

vector<vector<float>> values = {
    {0,0},        // Layer 0 (INPUT)
    {0,0,0},      // Layer 1
    {0}           // Lyaer 2 (OUTPUT)
};

vector<vector<float>> biases = {
    {0,0},              // Layer 0 (INPUT)
    {0.38,0.82,0.34},   // Layer 1
    {0}                 // Lyaer 2 (OUTPUT)
};

vector<vector<vector<float>>> weights = {
    {{1.4, -0.46, -0.85}, {0.65, -0.98, -0.21}},    // Layer 0 to 1
    {{1.6}, {-1.3}, {-0.87}},                       // Layer 1 to 2
};


float forwardPropagation(float input0, float input1)
{
    values[0][0] = input0;
    values[0][1] = input1;

    values[1][0] = activation( (values[0][0] * weights[0][0][0]) + (values[0][1] * weights[0][1][0]) + biases[1][0] );
    values[1][1] = activation( (values[0][0] * weights[0][0][1]) + (values[0][1] * weights[0][1][1]) + biases[1][1] );
    values[1][2] = activation( (values[0][0] * weights[0][0][2]) + (values[0][1] * weights[0][1][2]) + biases[1][2] );

    values[2][0] = activation( (values[1][0] * weights[1][0][0]) + (values[1][1] * weights[1][1][0]) + (values[1][2] * weights[1][2][0]) + biases[2][0] );

    return values[2][0];
}

int main()
{
    for (float y = 6; y >= -6; y-=0.5)
    {
        for (float x = -6; x <= 6; x+=0.5)
        {
            cout << (forwardPropagation(x, y) > 0.5 ? 1 : 0);
        }
        cout << endl;
    }
    return 0;
}