#include "neuronaut.hpp"
#include <iostream>

using namespace std;
using namespace neuronaut;

/*
	Example of building a network from a given list of biases and weights using the Network class and the buildNetwork function
*/

vector<float> biases = { 0,  2,0,0,  -3,0,0,  0};
vector<float> weights = { 0.75,0.5,-1,  1.5,0.2,-1.4,  0,-2,3,  0.5,-0.3,-1,  0.621, -1, 0.3 };
Network net = buildNetwork({1, 3,3, 1}, relu, biases, weights);

int main(int argc, char** argv)
{
	for (float i = -10 ; i <= 10 ; i += 0.10) cout << i << "\t" << net.forwardPropagation({i})[0] << endl;
	return 0;
}
