#include "neuronaut.hpp"
#include "neuroplot.hpp"
#include <iostream>
#include <csignal>

using namespace std;
using namespace neuronaut;
using namespace neuroplot;

/*
	Example of creating a network, training it with sample data.
	Once trained, the network should return outputs matching the sample data.
*/

// Samples provided for trainning. For each pair, the first value (the map's key) is the network's input,
// and the second value (the map's value) is the expected output for the given input.
map< vector<float>, vector<float> > samples = {
	{{-5}, {0}},
	{{-4}, {1}},
	{{-3}, {3}},
	{{-2}, {5}},
	{{-1}, {4}},
	{{ 0}, {3}},
	{{ 1}, {4}},
	{{ 2}, {5}},
	{{ 3}, {3}},
	{{ 4}, {1}},
	{{ 5}, {0}},
};

int main(int argc, char** argv)
{
	// Initialize the network with randomized parameters
	// Three layers of 8 neurons each
	Network net = initNetwork({1, 8,8,8, 1}, relu);

	// Display the un-trained network loss
	float initloss = calcLoss(net, samples);
	cerr << "Initial loss: " << initloss << endl ;

	// Train the Network
	DumbTrainning t(net, samples);
	float lastloss = initloss;
	float loss = t.trainIteration(0.001, lastloss);
	while (loss > 0.001 && loss < lastloss)
	{
		lastloss = loss;
		loss = t.trainIteration(0.001, lastloss);
		cerr << "loss: " << loss << endl;
	}

	cerr << "Initial loss: " << initloss << endl;
	cerr << "Final loss: " << loss << endl;

	// Display the network's output
	for (float i = -10 ; i <= 10 ; i += 0.5) cout << i << "\t" << net.forwardPropagation({i})[0] << endl;

	return 0;
}
