#include "neuronaut.hpp"
#include <stdexcept>
#include <cmath>
#include <iostream>
#include <fstream>
#include <future>

namespace neuronaut
{

using namespace std;

// ##### Random class #####

int Random::getInt(int min, int max)
{
	uniform_int_distribution<int> dist(min,max);
	return dist(rng);
}
int Random::operator()(int min, int max)
{
	return getInt(min,max);
}
float Random::getFloat(float min, float max)
{
	uniform_real_distribution<float> dist(min,max);
	return dist(rng);
}
float Random::operator()(float min, float max)
{
	return getFloat(min,max);
}


Random::Random(unsigned int seed) : rng(seed)
{
	// Nothing to do
}
Random::Random() : Random(random_device()())
{
	// Nothing to do
}

string toString(time_t t)
{
	char buffer[255];
	strftime(buffer, sizeof(buffer), "%FT%T%z", localtime(&t));
	return string(buffer);
}
vector<float> randomFloats(size_t amount, float min, float max)
{
	vector<float> results;
	Random r;
	for (int i = 0; i < amount; i++)  results.emplace_back(r(min, max));
	return move(results);
}
vector<float> slice(const vector<float>& v, size_t length, size_t offset)
{
	vector<float> result;
	for (size_t i = offset; i < offset + length; i++) result.emplace_back(v[i]);
	return result;
}

map< vector<float>,vector<float> > normalize(map<float,float> v)
{
	map< vector<float>,vector<float> > normalized;
	for ( auto& sample : v) normalized.emplace(pair<vector<float>,vector<float>>{vector<float>{sample.first},vector<float>{sample.second}});
	return normalized;
}
vector< map< vector<float>,vector<float> > > spread(map< vector<float>,vector<float> > v, size_t partitions)
{
	vector< map< vector<float>,vector<float> > > result(partitions);
	size_t datapart = 0;
	for (auto& sample : v)
	{
		result[datapart].emplace(sample);
		datapart++;
		if (datapart >= partitions) datapart = 0;
	}
	return result;
}
vector< pair< vector<float>, vector<float> > > shuffle(vector< pair< vector<float>, vector<float> > > v)
{
    vector< pair< vector<float>, vector<float> > > ret;
    Random r;
    while (v.size() > 0)
    {
        size_t i = r.get<size_t>(0, v.size() - 1);
        ret.emplace_back(v[i]);
        v.erase(v.begin() + i);
    }
    return ret;
}
string toString(const vector<float>& v)
{
	string ret;
	ret += "{";
	size_t lastIndex = v.size() - 1;
	for (size_t i = 0; i <= lastIndex; i++)
	{
		ret += v[i];
		if (i < lastIndex) ret += ",";
	}
	ret += "}";
	return ret;
}
ostream& operator<<(ostream& out,const vector<float>& v)
{
	out << "{";
	size_t lastIndex = v.size() - 1;
	for (size_t i = 0; i <= lastIndex; i++)
	{
		out << v.at(i);
		if (i < lastIndex) out << ",";
	}
	out << "}";
	return out;
}

bool isIn(char c, const string& s)
{
	return s.find(c) != string::npos;
}
string ltrim(const string& str, string toremove)
{
	for (size_t i = 0; i < str.size(); i++)
	{
		if (!isIn(str[i], toremove)) return str.substr(i, str.size() - i);
	}
	return str;
}
string rtrim(const string& str, string toremove)
{
	for (size_t i = str.size() - 1; i >= 0; i--)
	{
		if (!isIn(str[i], toremove)) return str.substr(0, i + 1);
	}
	return str;
}
string trim(const string& str, string toremove)
{
	return rtrim(rtrim(str));
}

vector<string> parseLine(const string& line, char fieldDelimiter)
{
	vector<string> ret;
	size_t tokenStart = 0;
	for (size_t i = 0; i < line.size(); i++)
	{
		if (line[i] == fieldDelimiter)
		{
			ret.emplace_back(line.substr(tokenStart, i - tokenStart));
			tokenStart = i+1;
		}
	}
	ret.emplace_back(line.substr(tokenStart, line.size() - tokenStart));
	return ret;
}

vector<vector<float>> loadFloatCsv(const string& filePath, char fieldDelimiter, float fillValue)
{
	vector<vector<float>> ret;
	ifstream ist(filePath);
	string line;
	while(getline(ist, line))
	{
		line = trim(line);
		if (line.empty()) continue;	  // skip empty lines
		if (line[0] == '#') continue;	// skip comment lines
		vector<float> row;
		for (string field : parseLine(line, fieldDelimiter))
		{
			field = trim(field);
			if (field.empty()) row.emplace_back(0);
			else row.emplace_back(stof(field));
		}
		ret.emplace_back(row);
	}

	// Find amount of columns
	size_t columns = 0;
	for (auto& v : ret) columns = max(columns, v.size());

	// Normalize the table
	for (auto& v : ret)
	{
		for (size_t i = v.size(); i < columns; i++) v.emplace_back(fillValue);
	}
	return ret;
}

map<vector<float>,vector<float>> loadSamples(const string& filePath, set<size_t> keys, char fieldDelimiter, float fillValue)
{
	map<vector<float>,vector<float>> ret;
	vector<vector<float>> rawSamples = loadFloatCsv(filePath, fieldDelimiter, fillValue);
	for (auto& row : rawSamples)
	{
		pair<vector<float>,vector<float>> newRow;
		for (int i = 0; i < row.size(); i++)
		{
			if (keys.contains(i)) newRow.first.emplace_back(row[i]);
			else newRow.second.emplace_back(row[i]);
		}
		ret.emplace(newRow);
	}
	return ret;
}

// ##### Activation functions #####

bool operator==(const ActivationFunction& fo, float(*fp)(float))
{
	return *fo.target<float(*)(float)>() == fp;
}
bool operator==(const ActivationFunction& f1, const ActivationFunction& f2)
{
	return *f1.target<float(*)(float)>() == *f2.target<float(*)(float)>();
}


float linear(float x)
{
	return x;
}
float linearDerivative(float x)
{
	return 1;
}
float relu(float x)
{
	if (x > 0) return x;
	return 0;
}
float reluDerivative(float x)
{
	if (x < 0) return 0;
	if (x > 0) return 1;
	return INFINITY;
}
float sigmoid(float x)
{
	return 1/(1+exp(-x));
}
float sigmoidDerivative(float x)
{
	return exp(-x) / pow(1 + exp(-x), 2);
}
float softplus(float x)
{
	return log(1+exp(x));
}
float softplusDerivative(float x)
{
	return 1/(1+exp(-x));
}
float tanh(float x)
{
	return (exp(x) - exp(-x)) / (exp(x) + exp(-x));
}
float tanhDerivative(float x)
{
	return 1 / pow(cosh(x), 2);
}

// ##### Loss functions #####

bool operator==(const LossFunction& fo, float(*fp)(float,float))
{
	return *fo.target<float(*)(float,float)>() == fp;
}
bool operator==(const LossFunction& f1, const LossFunction& f2)
{
	return *f1.target<float(*)(float,float)>() == *f2.target<float(*)(float,float)>();
}


float mse(float expected, float obtained)
{
	return pow(obtained - expected, 2);
}
float mseDerivative(float expected, float obtained)
{
	return 2 * (obtained - expected);
}
float halfMse(float expected, float obtained)
{
	return 0.5 * pow(obtained - expected, 2);
}
float halfMseDerivative(float expected, float obtained)
{
	return obtained - expected;
}

// ##### Other useful mathematical functions #####
float binaryStep(float x)
{
	if (x < 0) return 0;
	return 1;
}
float signum(float x)
{
	if (x > 0) return 1;
	if (x < 0) return -1;
	return 0;
}

ActivationFunction getDerivative(const ActivationFunction& f)
{
	if (f == relu) return reluDerivative;
	if (f == linear) return linearDerivative;
	if (f == tanh) return tanhDerivative;
	if (f == sigmoid) return sigmoidDerivative;
	if (f == softplus) return softplusDerivative;
	throw invalid_argument("There is not known derivative for the given function");
}

string toString(const ActivationFunction& funk)
{
	if (funk == linear) return "linear";
	if (funk == sigmoid) return "sigmoid";
	if (funk == relu) return "relu";
	if (funk == softplus) return "softplus";
	if (funk == neuronaut::tanh || funk == std::tanh) return "tanh";
	if (funk == binaryStep) return "binaryStep";
	if (funk == signum) return "signum";
	return "unknown";
}

// ##### Neuron class #####

Neuron::Neuron(ActivationFunction activation, float bias) : activation(activation), bias(bias), activationDerivative(getDerivative(activation))
{
	// Nothing to do
}
Neuron::Neuron(const Neuron& rhs) = default;
Neuron::Neuron(Neuron&& rhs) = default;

Neuron& Neuron::operator =(const Neuron& rhs) = default;
Neuron& Neuron::operator =(Neuron&&) noexcept = default;


// ##### Network class #####

vector<float> Network::forwardPropagation(const vector<float>& inputValues)
{
	if (layers.size() < 1) throw runtime_error("The network doesn't have an input layer");
	if (layers.front().size() < 1) throw runtime_error("The network doesn't have neurons in the input layer");
	if (inputValues.size() != layers.front().size()) throw runtime_error("Input values count ("s + to_string(inputValues.size()) + ") doesn't match input neurons count ("s + to_string(layers.front().size()) + ")"s);

	// Input layer
	for (size_t n = 0; n < layers[0].size(); n++)
	{
		Neuron& neuron = layers[0][n];
		neuron.input  = inputValues[n] + neuron.bias;
		neuron.output = neuron.activation(neuron.input);
		neuron.input = neuron.output = inputValues[n];
	}
	// Hidden and output layers
	for (size_t l = 1; l < layers.size(); l++)
	{
		size_t prevL = l - 1;
		for (size_t n = 0; n < layers[l].size(); n++)
		{
			Neuron& neuron = layers[l][n];
			neuron.input = neuron.bias;
			for (size_t prevN = 0; prevN < layers[prevL].size(); prevN++)
			{
				Neuron& prevNeuron = layers[prevL][prevN];
				neuron.input += prevNeuron.output * prevNeuron.weights[n];
			}
			neuron.output = neuron.activation(neuron.input);
		}
	}
	vector<float> outputValues;
	Layer& lastLayer = layers.back();
	for (size_t i = 0; i < lastLayer.size(); i++) outputValues.emplace_back(lastLayer[i].output);
	return outputValues;
}
void Network::setBias(const vector<float>& biases)
{
	size_t b = 0;
	for (auto& layer : layers)
	{
		for (auto& neuron : layer) neuron.bias = biases[b++];
	}
}
void Network::setWeights(const vector<float>& weights)
{
	size_t w = 0;
	for (size_t l = 0; l < layers.size() - 1; l++)
	{
		size_t connections = layers[l+1].size();
		for (Neuron& neuron: layers[l])
		{
			neuron.weights.clear();
			for (size_t c = 0; c < connections; c++) neuron.weights.emplace_back(weights[w++]);
			neuron.weightsDeltas = neuron.weightsDeltasAccumulated = vector<float>(neuron.weights.size(), 0);
		}
	}
}
void Network::setActivations(vector<ActivationFunction> activations)
{
	size_t a = 0;
	for (auto& layer : layers)
	{
		for (auto& neuron : layer)
		{
			neuron.activation = activations[a++];
			neuron.activationDerivative = getDerivative(neuron.activation);
		}
	}
}
vector<float> Network::getBias() const
{
	vector<float> result;
	for (auto& layer : layers)
	{
		for (auto& neuron : layer) result.emplace_back(neuron.bias);
	}
	return result;
}
vector<float> Network::getWeights() const
{
	vector<float> ret;
	for (const Layer& layer : layers)
	{
		for (const Neuron& neuron : layer)
		{
			for (float weight : neuron.weights) ret.emplace_back(weight);
		}
	}
	return ret;
}
vector<ActivationFunction> Network::getActivations() const
{
	vector<ActivationFunction> result;
	for (auto& layer : layers)
	{
		for (auto& neuron : layer) result.emplace_back(neuron.activation);
	}
	return result;
}

void Network::backPropagation(const vector<float>& expected, const vector<float>& results)
{
	Layer& lastLayer = layers.back();
	for (size_t n = 0; n < lastLayer.size(); n++)
	{
		Neuron& neuron = lastLayer[n];
		neuron.delta = halfMseDerivative( expected[n], results[n]);
		//neuron.deltaAcummulated += neuron.delta;
	}

	for (size_t l = layers.size() - 1; l > 0; l--)
	{
		size_t prevL = l - 1;
		Layer& layer = layers[l];
		Layer& prevLayer =  layers[l-1];
		for (size_t n = 0; n < layer.size(); n++)
		{
			Neuron& neuron = layer[n];
			neuron.delta = neuron.delta * neuron.activationDerivative(neuron.input);
			neuron.deltaAcummulated += neuron.delta;
			for (size_t prevN = 0; prevN < prevLayer.size(); prevN++)
			{
				Neuron& prevNeuron = prevLayer[prevN];
				prevNeuron.weightsDeltas[n] = neuron.delta * prevNeuron.output;
				prevNeuron.weightsDeltasAccumulated[n] += prevNeuron.weightsDeltas[n];
				if (n == 0) prevNeuron.delta = 0;
				prevNeuron.delta += neuron.delta * prevNeuron.weights[n];
			}
		}
	}

	Layer& firstLayer = layers.front();
	for (size_t n = 0; n < firstLayer.size(); n++)
	{
		Neuron& neuron = firstLayer[n];
		neuron.delta = neuron.delta * neuron.activationDerivative(neuron.input);
		neuron.deltaAcummulated += neuron.delta;
	}

	accumulated++;
}

void Network::gradientDescend(float learningRate)
{
	size_t lastL = layers.size() - 1;
	for (size_t l = 0; l <= lastL; l++)
	{
		Layer& layer = layers[l];
		for (size_t n = 0; n < layer.size(); n++)
		{
			Neuron& neuron = layer[n];
			neuron.bias -= learningRate * (neuron.deltaAcummulated / accumulated);
			neuron.deltaAcummulated = 0;
			if (l >= lastL) continue;

			for (size_t w = 0; w < neuron.weightsDeltasAccumulated.size(); w++)
			{
				neuron.weights[w] -= learningRate * (neuron.weightsDeltasAccumulated[w] / accumulated);
				neuron.weightsDeltasAccumulated[w] = 0;
			}
		}
	}
	accumulated = 0;
}

void Network::trainBatch(float learningRate, const vector<pair<vector<float> ,vector<float>>>& samples)
{
	for (size_t s = 0; s < samples.size(); s++)
	{
		auto& sample = samples[s];
		vector<float> inputs = sample.first;
		vector<float> expectedOutputs = sample.second;
		vector<float> outputs = forwardPropagation(inputs);
		backPropagation(expectedOutputs, outputs);
	}
	gradientDescend(learningRate);
}


Network::Network() = default;
Network::Network(const Network& rhs)
{
	*this = rhs;
}
Network::Network(Network&& rhs) = default;

Network& Network::operator =(const Network& rhs) noexcept
{
	layers.clear();
	layers = rhs.layers;
	this->setWeights(rhs.getWeights());
	return *this;
}
Network& Network::operator =(Network&& rhs) noexcept = default;

size_t calculateNeuronsAmount(const vector<size_t>& topology)
{
	return sum(topology);
}
size_t calculateConnectionsAmount(const vector<size_t>& topology)
{
	size_t result = 0;
	for (ssize_t i = 0; i < (ssize_t) topology.size() - 1; i++) result += topology[i] * topology[i+1];
	return result;
}

Network buildNetwork(const vector<size_t>& topology, ActivationFunction activation)
{
	auto amountNeurons = calculateNeuronsAmount(topology);
	auto amountConnections = calculateConnectionsAmount(topology);
	vector<float> biases(amountNeurons,0);
	vector<float> weights(amountConnections, 1);
	return move(buildNetwork(topology, activation, biases, weights));
}

Network buildNetwork(const vector<size_t>& topology, ActivationFunction activation, const vector<float>& biases, const vector<float>& weights)
{
	vector<ActivationFunction> activations(calculateNeuronsAmount(topology), activation);
	for (size_t i = 0; topology.size() > 0 && i < topology.front(); i++) activations[i] = linear;
	for (size_t i = activations.size() - topology.back(); topology.size() > 1 && i < activations.size(); i++) activations[i] = linear;
	return buildNetwork(topology, activations, biases, weights);
}
Network buildNetwork(const vector<size_t>& topology, const vector<ActivationFunction>& activation, const vector<float>& biases, const vector<float>& weights)
{
	// Checks
	if (biases.size() < calculateNeuronsAmount(topology)) throw runtime_error("Less biases values provided than neurons in the network");
	if (weights.size() < calculateConnectionsAmount(topology)) throw runtime_error("Less weigth values provied than connections in the network");
	if (activation.size() < calculateNeuronsAmount(topology)) throw runtime_error("Less activation values provided than neurons in the network");

	Network net;
	size_t i = 0;
	for (size_t l = 0; l < topology.size(); l++)
	{
		Layer layer;
		for (size_t n = 0; n < topology[l]; n++)
		{
			layer.emplace_back(Neuron(activation[i], biases[i]));
			i++;
		}
		net.layers.emplace_back(layer);
	}
	net.setWeights(weights);

	return move(net);
}

Network initNetwork(const vector<size_t>& topology, ActivationFunction activation, float minWeight, float maxWeight)
{
	vector<float> biases  = randomFloats( calculateNeuronsAmount(topology) , minWeight, maxWeight);
	vector<float> weights = randomFloats( calculateConnectionsAmount(topology) , minWeight, maxWeight);
	return buildNetwork( topology, activation, biases, weights);
}

Network initNetwork(const vector<size_t>& topology, const vector<ActivationFunction>& activationFunctions, float minWeight, float maxWeight)
{
	size_t neurons = calculateNeuronsAmount(topology);
	size_t connections = calculateConnectionsAmount(topology);
	vector<float> biases  = randomFloats( neurons, minWeight, maxWeight);
	vector<float> weights = randomFloats( connections, minWeight, maxWeight);

	Random random;
	vector<ActivationFunction> activations;
	size_t i = 0;
	for (; i < topology.front(); i++) activations.emplace_back(linear);
	for (; i < neurons - topology.back(); i++)
	{
		ActivationFunction funk = activationFunctions[ random.getInt(0, activationFunctions.size() - 1)];
		activations.emplace_back(funk);
	}
	for (; i < neurons; i++) activations.emplace_back(linear);

	return buildNetwork( topology, activations, biases, weights);
}


void save(Network& net, const string& filePath)
{
	vector<float> biases = net.getBias();
	vector<float> weights = net.getWeights();
	vector<ActivationFunction> activations = net.getActivations();
	vector<unsigned char> activationCodes;
	for (auto& f : activations)
	{
		unsigned char code = 0;
		if (f == linear)	 code = 0;
		if (f == relu)	   code = 1;
		if (f == sigmoid)	code = 2;
		if (f == binaryStep) code = 3;
		if (f == signum)	 code = 4;
		if (f == softplus)   code = 5;
		if (f == tanh)	   code = 6;
		activationCodes.emplace_back(code);
	}
	vector<size_t> topology;
	for (Layer& l : net.layers) topology.emplace_back(l.size());

	ofstream ofs(filePath, ofstream::binary | ofstream::trunc);
	if (!ofs.good()) throw runtime_error("Error openning file '"s + filePath + "' for writing"s);
	size_t layers = net.layers.size();
	ofs.write((const char*) &layers, sizeof(layers));
	ofs.write((const char*) topology.data(), sizeof(size_t) * topology.size());
	ofs.write((const char*) activationCodes.data(), sizeof(unsigned char) * activationCodes.size());
	ofs.write((const char*) biases.data(), sizeof(float) * biases.size());
	ofs.write((const char*) weights.data(), sizeof(float) * weights.size());
	ofs.flush();
	ofs.close();
}

Network load(const string& filePath)
{
	ifstream in(filePath, ifstream::binary);
	if (!in.good()) throw runtime_error("Error openning file '"s + filePath + "' for reading"s);
	size_t layers = 0;
	in.read((char*) &layers, sizeof(layers));

	vector<size_t> topology(layers);
	in.read((char*) topology.data(), sizeof(size_t) * layers);

	size_t neurons	 = calculateNeuronsAmount(topology);
	size_t connections = calculateConnectionsAmount(topology);

	vector<unsigned char> activationCodes(neurons);
	in.read((char*) activationCodes.data(), sizeof(unsigned char) * neurons);

	vector<ActivationFunction> activations;
	for (unsigned char code : activationCodes)
	{
		ActivationFunction funk = linear;
		if (code == 0) funk = linear;
		if (code == 1) funk = relu;
		if (code == 2) funk = sigmoid;
		if (code == 3) funk = binaryStep;
		if (code == 4) funk = signum;
		if (code == 5) funk = softplus;
		if (code == 6) funk = tanh;
		activations.emplace_back(funk);
	}

	vector<float> biases(neurons);
	in.read((char*) biases.data(), sizeof(float) * neurons);

	vector<float> weights(connections);
	in.read((char*) weights.data(), sizeof(float) * connections);

	return buildNetwork(topology, activations, biases, weights);
}

void print(Neuron& neuron, ostream& out)
{
	out << "\tNeuron address: '" << &neuron << "', bias: " << neuron.bias << ", activation: " << toString(neuron.activation) << ", input: " << neuron.input << ", output: " << neuron.output << endl;
	for (size_t w = 0; w < neuron.weights.size(); w++)
	{
		out << "\t\tconnection to '" << w << "', weight: " << neuron.weights[w] << endl;
	}
}

void print(Network& net, ostream& out)
{
	out << "NETWORK" << endl;
	out << "\tLayers: " << net.layers.size() << endl;
	if (net.layers.size() > 0)
	{
		out << "\tNeurons in each layer:";
		for (Layer& layer : net.layers)
		{
			out << " " << layer.size();
		}
	}
	out << endl;
	out << endl;

	for (size_t i = 0; i < net.layers.size(); i++)
	{
		out << "LAYER " << i << endl;
		for (Neuron& neuron : net.layers[i]) print(neuron, out);
	}
}
void print(Network& net)
{
	print(net, cout);
}


// ##### Trainning #####

float calcLoss(Network& net, const map<float, float>& samples)
{
	float sumloss = 0;
	for (auto& p : samples)
	{
		float given = p.first;
		float expected = p.second;
		float result = net.forwardPropagation({given})[0];
		float loss = halfMse(expected, result);
		sumloss += loss;
	}
	return sumloss / samples.size();
}
float calcLoss(Network& net, const map<vector<float>, vector<float>>& samples)
{

	float sumloss = 0;
	for (auto& p : samples)
	{
		vector<float> given = p.first;
		vector<float> expected = p.second;
		vector<float> results = net.forwardPropagation(given);
		float loss = 0;
		for (size_t i = 0; i < results.size(); i++) loss += halfMse(expected[i], results[i]);
		sumloss += (loss/results.size());
	}
	return sumloss / samples.size();
}


float DumbTrainning::calcLoss()
{
	return neuronaut::calcLoss(net, samples);
}

float DumbTrainning::tuneWeight(Neuron& neuron, size_t connection, float learningRate, float initloss)
{
	// try sum
	neuron.weights[connection] += learningRate;
	float loss = calcLoss();
	if (loss < initloss) return loss;
	else neuron.weights[connection] -= learningRate; // Revert change

	// try sub
	neuron.weights[connection] -= learningRate;
	loss = calcLoss();
	if (loss < initloss) return loss;
	else neuron.weights[connection] += learningRate; // Revert change

	// none worked
	return initloss;
}

float DumbTrainning::tuneBias(Neuron& neuron, float learningRate, float initloss)
{
	// try sum
	neuron.bias += learningRate;
	float loss = calcLoss();
	if (loss < initloss) return loss;
	else neuron.bias -= learningRate; // Revert change

	// try sub
	neuron.bias -= learningRate;
	loss = calcLoss();
	if (loss < initloss) return loss;
	else neuron.bias += learningRate; // Revert change

	// none worked
	return initloss;
}

float DumbTrainning::tuneNeuron(Neuron& neuron, float learningRate, float initloss)
{
	float bestloss = initloss;
	for (size_t connection = 0; connection < neuron.weights.size(); connection++)
	{
		bestloss = tuneWeight(neuron, connection, learningRate, bestloss);
	}
	bestloss = tuneBias(neuron, learningRate, bestloss);
	return bestloss;
}

float DumbTrainning::tuneLayer(Layer& layer, float learningRate, float initloss)
{
	float bestloss = initloss;
	for (Neuron& neuron : layer) bestloss = tuneNeuron(neuron, learningRate, bestloss);
	return bestloss;
}

float DumbTrainning::tuneNetwork(float learningRate, float initloss)
{
	float bestloss = initloss;
	for (Layer& layer : net.layers) bestloss = tuneLayer(layer, learningRate, bestloss);
	return bestloss;
}

float DumbTrainning::trainIteration(float learningRate, float initloss)
{
	return tuneNetwork(learningRate, initloss);
}
DumbTrainning::DumbTrainning(Network& net, const map< vector<float>,vector<float> >& samples) : net(net), samples(samples)
{

}



void GradientDescendTrainning::trainIteration(float learningRate, size_t batchSize)
{
	if (cursample >= samples.size())
	{
		cursample = 0;
		samples = shuffle(samples);
		epoch++;
	}
	vector< pair< vector<float>, vector<float> > > batch;
	for (size_t i = 0; i < batchSize && cursample < samples.size(); i++)
	{
		batch.push_back(samples[cursample++]);
	}
	net.trainBatch(learningRate, batch);
	iteration++;
}
GradientDescendTrainning::GradientDescendTrainning(Network& net, const map< vector<float>,vector<float> >& samples) : net(net), originalSamples(samples)
{
	this->samples = vector< pair<vector<float>,vector<float> > >(originalSamples.begin(), originalSamples.end());
	this->samples = shuffle(this->samples);
}

} // namespace
